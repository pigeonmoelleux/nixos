{ pkgs, ... }:

{
  users = {
    mutableUsers = false;

    users = {
      root = {
        hashedPassword = "*";
      };

      ratcornu = {
        isNormalUser = true;
        extraGroups = [ "wheel" ];

        home = "/home/ratcornu";
        homeMode = "711";

        packages = with pkgs; [
          shelldap
          yt-dlp
        ];

        hashedPassword = "$y$j9T$TiCw1hCIYnx1MLQNuj7Po.$G4Z4.arsvS0eyOoCRh9bHW.RxcErDs.6DYj2NsdAi2.";
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2QIWetjGv9fwL7nrC7chdIaRQ1RH79/YHq6a3Fm4bQ2/yUm97BLsKETP1C+t5p4b383x3LQnD1fk2T7kL0CrGRAK5afRisDRzkJ+sGhrPmQQF7Yil7Cg2MKmXsJHZZr4dmdxE0PlEFhx6Omoybq9yV97Qd43NI+slA44fIVAW/dMYAbJOwDMt6aF/uBHvYlva7V+bWYVqMDo1A2MrRf/E2FgRPJFH/2dI8kunYTifm4zMRE50E2f8MYQ7TauMHnQ+L1wrUQj53ZlWRgVbhUdj1EbnG9avj1q1wRvps0DEKAqXPxB8Z8zLL/7Ln+8bPv6dr5ljYRHuc3I+3hUY8TVRnaDggDj5Up+Cvyj0DVv8N7eQhp3UxtdZ6FhZuNKikC1Uym9ChoG76n25SKclZHu30IE5GIpzXZSGnnkOmYKoVkRMNeu4PNYTlYS7UnSKQJatUXCMMSvIgv5PS7JUfe5run+6xfAG6CgIQ/jRMqCwOyu3BSzqy71Nep/gRhPs8J/BI/8tse7adAxzr2fs5QiAgfHzXDzLWnKUkpZ1tCDrZSz+lfi6OLV9GMBVWlqcdXS9RvvHwp1pNjpwGZFOw2lhPz/VpE9EAap2omYNC8Fssp3DrrVjq7LsznuRINf9Jl5bjdTl4ksbh6fiVY9HKYZ/pEQYLsUptFgZ2LjeCRQ5fw== openpgp:0x520114C6"
        ];
      };
    };
  };
}
