{
  inputs,
  lib,
  pkgs,
  ...
}:

{
  imports = [
    ./locale.nix
    ./programs.nix
    ./users.nix
  ];

  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      auto-optimise-store = true;
    };

    package = pkgs.nixVersions.latest;
  };

  boot.tmp = {
    useTmpfs = true;
    cleanOnBoot = true;
  };
}
