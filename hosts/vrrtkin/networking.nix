{ pkgs, lib, ... }:

{
  services.resolved.enable = true;

  networking = {
    wireguard = {
      enable = false;
    };
  };

  services.mullvad-vpn = {
    enable = true;
    package = pkgs.mullvad-vpn;
  };
}
