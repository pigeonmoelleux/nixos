{
  config,
  pkgs,
  ...
}:

{
  imports = [
    ./audio.nix
    ./desktop.nix
    ./hardware-configuration.nix
    ./networking.nix
    ./nvidia.nix
    ./syncthing.nix
  ];

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;

      grub = {
        enable = true;
        efiSupport = true;
        device = "nodev";
        configurationLimit = 5;

        extraEntries = ''
          menuentry "Windows" {
            insmod part_gpt
            insmod fat
            insmod search_fs_uuid
            insmod chain
            search --fs-uuid --set=root 7A09-E7FD
            chainloader /EFI/Microsoft/Boot/bootmgfw.efi
          }
        '';
        extraEntriesBeforeNixOS = true;
      };
    };

    initrd = {
      kernelModules = [
        "amdgpu"
        "nvidia"
        "nvidia_modeset"
        "nvidia_uvm"
        "nvidia_drm"
      ];
    };

    kernelParams = [
      "processor.max_cstate=4"
      "amd_iommu=on"
      "idle=nowait"
      "iommu=pt"
      "pcie_acs_override=downstream,multifunction"
    ];
    kernelPackages = pkgs.linuxPackages_6_12;

    extraModulePackages = [
      config.boot.kernelPackages.nvidia_x11_beta
    ];

    blacklistedKernelModules = [
      "i2c_nvidia_gpu"
    ];

    binfmt.emulatedSystems = [ "aarch64-linux" ];
  };

  hardware = {
    graphics.enable = true;
  };
  time.hardwareClockInLocalTime = true;

  networking.hostName = "vrrtkin";
  networking.hostId = "ef530913";
  networking.networkmanager.enable = true;

  services.xserver = {
    enable = true;

    xkb = {
      variant = "oss";
      layout = "fr";
    };

    videoDrivers = [
      "amdgpu"
      "nvidia"
    ];
  };

  services.printing.enable = true;

  nixpkgs.config.allowUnfree = true;

  programs.gnupg.agent = {
    pinentryPackage = pkgs.pinentry-gnome3;
  };
  services.yubikey-agent.enable = true;
  services.udev.packages = [ pkgs.yubikey-personalization ];
  services.pcscd.enable = true;

  security.pam.services.swaylock = { };

  users.users.ratcornu.extraGroups = [
    "adbusers"
    "audio"
  ];
  programs.adb = {
    enable = true;
  };

  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
    "${config.users.users.ratcornu.home}/.ssh/id_ed25519"
  ];

  system.stateVersion = "24.11"; # Did you read the comment?

}
