{ ... }:

{
  desktop = {
    gnome.enable = true;
    input.enable = true;

    games = {
      steam.enable = true;
      sunshine.enable = true;
    };
    virtualisation = {
      enable = true;
      waydroid.enable = true;
    };
  };
}
