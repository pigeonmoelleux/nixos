{ pkgs, ... }:

{
  networking = {
    wireguard = {
      enable = false;
    };
  };

  services.mullvad-vpn = {
    enable = true;
    package = pkgs.mullvad-vpn;
  };
}
