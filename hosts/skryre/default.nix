{
  config,
  pkgs,
  lib,
  ...
}:

{
  imports = [
    ./desktop.nix
    ./hardware-configuration.nix
    ./networking.nix
    ./nvidia.nix
    ./syncthing.nix
  ];

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;

      grub = {
        enable = true;
        efiSupport = true;
        zfsSupport = true;
        device = "nodev";
        configurationLimit = 5;
      };
    };

    initrd = {
      kernelModules = [
        "amdgpu"
        "nvidia"
        "nvidia_modeset"
        "nvidia_uvm"
        "nvidia_drm"
      ];
    };

    zfs = {
      package = pkgs.zfs_unstable;
      extraPools = [ "clan" ];
      forceImportAll = true;
    };

    kernelParams = [
      "processor.max_cstate=4"
      "amd_iommu=on"
      "idle=nowait"
      "iommu=pt"
      "pcie_acs_override=downstream,multifunction"
    ];
    kernelPackages = pkgs.linuxPackages_6_12;

    extraModulePackages = [
      config.boot.kernelPackages.nvidia_x11_beta
    ];

    blacklistedKernelModules = [
      "i2c_nvidia_gpu"
    ];

    binfmt.emulatedSystems = [ "aarch64-linux" ];
  };

  hardware.graphics.enable = true;

  vfio = {
    gpuIDs = [
      "10de:2191"
      "10de:1aeb"
    ];
  };

  specialisation."VFIO".configuration = {
    system.nixos.tags = [ "with-vfio" ];
    vfio.enable = true;
  };

  networking.hostName = "skryre";
  networking.hostId = "ef531309";
  networking.networkmanager.enable = true;

  powerManagement.resumeCommands = ''
    ${pkgs.utillinux}/bin/rfkill unblock wlan
  '';

  services.xserver = {
    enable = true;

    xkb = {
      variant = "oss";
      layout = "fr";
    };

    videoDrivers = [
      "amdgpu"
      "displaylink"
      "nvidia"
    ];
  };

  services.printing.enable = true;

  nixpkgs.config.allowUnfree = true;

  programs.gnupg.agent = {
    pinentryPackage = pkgs.pinentry-gnome3;
  };
  services.yubikey-agent.enable = true;
  services.udev.packages = [ pkgs.yubikey-personalization ];
  services.pcscd.enable = true;

  security.pam.services.swaylock = { };

  users.users.ratcornu.extraGroups = [ "adbusers" ];
  programs.adb = {
    enable = true;
  };

  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
    "${config.users.users.ratcornu.home}/.ssh/id_ed25519"
  ];

  system.stateVersion = "24.11"; # Did you read the comment?

}
