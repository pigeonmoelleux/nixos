{ pkgs, inputs, ... }:

{
  services.openssh.enable = true;

  virtualisation.docker.enable = true;

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    viAlias = true;
  };

  programs.mtr.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    enableExtraSocket = true;
  };

  environment.systemPackages = with pkgs; [
    bat
    dig
    ffmpeg
    file
    git
    htop
    inetutils
    killall
    ncdu
    tree
    unzip
    wget

    inputs.agenix.packages.${system}.default
  ];
}
