{ pkgs, ... }:

{

  security.acme = {
    acceptTerms = true;
    defaults.email = "balthazar.patiachvili@crans.org";
  };

  users.users.nginx.extraGroups = [ "medias" ];
  services.nginx = {
    enable = true;

    additionalModules = with pkgs.nginxModules; [
      geoip2
    ];

    recommendedProxySettings = true;
    recommendedOptimisation = true;
    clientMaxBodySize = "16G";

    virtualHosts = {
      "volkn.skaven.org" = {
        enableACME = true;
        forceSSL = true;

        locations = {
          "/" = {
            proxyPass = "https://skaven.org";
          };
        };
      };
    };
  };
}
