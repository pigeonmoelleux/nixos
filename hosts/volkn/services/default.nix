{ ... }:

{
  imports = [
    ./nginx.nix
    ./restic.nix
    ./syncthing.nix
  ];
}
