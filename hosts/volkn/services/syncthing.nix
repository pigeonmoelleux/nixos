{ ... }:

let
  port = 8384;
in

{
  services.syncthing = {
    enable = true;
    enableVersioning = true;

    guiAddress = "0.0.0.0:${toString port}";
    openDefaultPorts = true;

    dataDir = "/var/lib/syncthing";

    settings = {
      gui = {
        theme = "black";
        user = "ratcornu";
        password = "$2a$10$cnNPmhPBBrHV8uB82co7sOr.x1mz/cELGlXyE6g8wrnWV/GKpWdoa";
      };
    };
  };

  systemd.tmpfiles.rules = [
    "d /home/ratcornu/musiques 2775 ratcornu syncthing"
  ];
}
