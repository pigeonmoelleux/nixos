{ config, ... }:

{
  services.restic = {
    server = {
      enable = true;

      listenAddress = "13009";
      privateRepos = true;
    };
  };

  services.nginx.virtualHosts = {
    "restic.volkn.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://localhost:${config.services.restic.server.listenAddress}";
      };
    };
  };
}
