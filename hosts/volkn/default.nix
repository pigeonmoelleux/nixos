{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix
    ./services
  ];

  boot = {
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };
  };

  nix.settings.trusted-users = [ "@wheel" ];
  hardware.enableRedistributableFirmware = true;

  networking = {
    hostName = "volkn";
    wireless.enable = false;
  };

  hardware.pulseaudio.enable = true;

  services.yubikey-agent.enable = true;
  services.udev.packages = [ pkgs.yubikey-personalization ];
  services.pcscd.enable = true;

  swapDevices = [
    {
      device = "/swapfile";
      size = 1024;
    }
  ];

  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
    "${config.users.users.ratcornu.home}/.ssh/id_ed25519"
  ];

  system.stateVersion = "23.11";
}
