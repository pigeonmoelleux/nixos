{ ... }:

{
  networking = {
    firewall = {
      enable = true;
      allowedTCPPorts = [
        22
        80
        443
      ];
      allowedUDPPorts = [
        80
        123
        443
      ];
      checkReversePath = "loose";
    };

    wireguard = {
      enable = false;
    };
  };

  services.fail2ban = {
    enable = true;
  };
}
