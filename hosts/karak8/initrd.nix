{ config, ... }:

let
  interface = "eno1";
in

{
  fileSystems."/home".options = [ "x-systemd.device-timeout=infinity" ];
  fileSystems."/unv".options = [ "x-systemd.device-timeout=infinity" ];
  fileSystems."/var".options = [ "x-systemd.device-timeout=infinity" ];

  boot.initrd = {
    availableKernelModules = [
      "ccm"
      "ctr"
      "r8169"
    ];

    systemd = {
      enable = true;

      users.root.shell = "/bin/systemd-tty-ask-password-agent";

      network = {
        enable = true;
        networks."10-wired" = {
          matchConfig.Name = interface;
          networkConfig.DHCP = "yes";
        };
      };
    };

    network = {
      enable = true;

      ssh = {
        port = 22;
        hostKeys = [ "/etc/ssh/ssh_host_ed25519_key" ];
        authorizedKeys = config.users.users."ratcornu".openssh.authorizedKeys.keys;
      };
    };
  };
}
