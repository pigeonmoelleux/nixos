{ config, ... }:

let
  port = 8384;
in

{
  users.users.syncthing.extraGroups = [
    "medias"
  ];

  services.syncthing = {
    enable = true;
    enableVersioning = true;

    group = "medias";

    guiAddress = "0.0.0.0:${toString port}";
    openDefaultPorts = true;
    dataDir = "/var/lib/syncthing";

    settings = {
      gui = {
        theme = "black";
        user = "ratcornu";
        password = "$2a$10$1gj/fE6eUvv17.xuHc4ed.TquCnMQyWajGEBSOOJgc2NwvvxvYKKG";
      };
    };
  };

  services.nginx.virtualHosts = {
    "syncthing.karak8.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://localhost:${toString port}";
      };
    };
  };
}
