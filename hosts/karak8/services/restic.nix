{ config, ... }:

{
  services.restic = {
    server = {
      enable = true;

      dataDir = "/unv/backups/restic";
      listenAddress = "13009";
      privateRepos = true;
    };
  };

  services.nginx.virtualHosts = {
    "restic.karak8.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://localhost:${config.services.restic.server.listenAddress}";
      };
    };
  };
}
