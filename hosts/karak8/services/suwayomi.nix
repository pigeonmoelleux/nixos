{ pkgs, config, ... }:

{
  age.secrets = {
    suwayomi-pass = {
      file = ../../../secrets/karak8/services/suwayomi-pass.age;
      owner = config.services.suwayomi-server.user;
    };
  };

  services.suwayomi-server = {
    enable = true;
    package = pkgs.ratcornu.suwayomi-server;

    group = "medias";
    openFirewall = true;

    settings = {
      server = {
        ip = config.networking.vpn-netns.vethIP;
        port = 4568;

        extensionRepos = [
          "https://raw.githubusercontent.com/keiyoushi/extensions/repo/index.min.json"
        ];

        downloadAsCbz = true;

        basicAuthEnabled = true;
        basicAuthUsername = "ratcornu";
        basicAuthPasswordFile = config.age.secrets.suwayomi-pass.path;

        backupPath = "/unv/backups/suwayomi";
        backupTime = "07:11";

        localSourcePath = "/unv/mangas/local";
      };
    };
  };

  fileSystems = {
    "/var/lib/suwayomi-server/.local/share/Tachidesk/downloads/mangas" = {
      device = "/unv/mangas";
      options = [ "bind" ];
    };
  };

  services.nginx = {
    virtualHosts."suwayomi.karak8.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://$suwayomi_upstream";
      };
    };

    appendHttpConfig = ''
      geo $suwayomi_upstream {
        default suwayomi_default_upstream;
        192.168.1.0/24 suwayomi_allowed_upstream;
        10.13.9.0/24 suwayomi_allowed_upstream;
        94.239.68.176/32 suwayomi_allowed_upstream;
      }
    '';

    upstreams = {
      suwayomi_default_upstream.servers = {
        "karak8.skaven.org" = { };
      };
      suwayomi_allowed_upstream.servers = {
        "${config.services.suwayomi-server.settings.server.ip}:${toString config.services.suwayomi-server.settings.server.port}" =
          { };
      };
    };
  };
}
