{ ... }:

{
  imports = [
    ./nginx.nix
    ./restic.nix
    ./suwayomi.nix
    ./syncthing.nix
  ];
}
