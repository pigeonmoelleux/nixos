{
  config,
  pkgs,
  lib,
  ...
}:

{
  imports = [
    ./hardware-configuration.nix
    ./initrd.nix
    ./networking
    ./services
  ];

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;

      grub = {
        enable = true;
        efiSupport = true;
        zfsSupport = true;
        device = "nodev";
        configurationLimit = 5;
      };
    };

    zfs = {
      extraPools = [ "clan" ];
      forceImportAll = true;
    };

    kernelPackages = pkgs.linuxPackages;
  };

  hardware.graphics.enable = true;

  networking.hostName = "karak8";
  networking.hostId = "bdad0913";
  networking.networkmanager.enable = true;

  nixpkgs.config.allowUnfree = true;

  services.yubikey-agent.enable = true;
  services.pcscd.enable = true;

  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
    "${config.users.users.ratcornu.home}/.ssh/id_ed25519"
  ];

  users = {
    motdFile = "/etc/motd";

    groups.medias = { };
  };

  system.stateVersion = "24.05"; # Did you read the comment?

}
