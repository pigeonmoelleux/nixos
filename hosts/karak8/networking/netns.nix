{ ... }:

{
  networking.vpn-netns = {
    wireguardInterface = "terrier";
    nameserver = "10.2.0.1";

    interfaceNamespace = "netns-terrier";
    vethInterfaceName = "veth-terrier";

    vethIP = "192.168.2.2";
    vethOuterIP = "192.168.2.1";

    wireguardOptions = {
      privateKeyFile = "/etc/wireguard/proton.key";
      ips = [ "10.2.0.2/32" ];

      peers = [
        {
          publicKey = "7FslkahrdLwGbv4QSX5Cft5CtQLmBUlpWC382SSF7Hw=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "103.125.235.19:51820";
          persistentKeepalive = 25;
        }
      ];
    };

    restrictedServices = [
      "suwayomi-server"
    ];

    portForwarding = {
      enable = true;
    };
  };
}
