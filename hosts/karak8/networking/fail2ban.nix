{ ... }:

{
  services.fail2ban = {
    enable = true;

    ignoreIP = [
      "185.230.78.0/23"
      "192.168.1.0/24"
    ];
  };
}
