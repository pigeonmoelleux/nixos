{ lib, ... }:

{
  networking.wireguard = {
    enable = true;

    interfaces = {
      tunnels = {
        privateKeyFile = "/etc/wireguard/private.key";
      };
    };
  };
}
