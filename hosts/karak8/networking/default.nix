{ config, lib, ... }:

{
  imports = [
    ./fail2ban.nix
    ./netns.nix
    ./vpn.nix
  ];

  networking = {
    domain = "karak8.skaven.org";
    extraHosts = "suwayomi.karak8.skaven.org";

    nameservers = [
      "9.9.9.9"
      "208.67.220.220"
      "208.67.222.222"
    ];

    enableIPv6 = false;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        22
        80
        443
      ];
      allowedUDPPorts = [
        80
        443
      ];

      checkReversePath = "loose";
      trustedInterfaces = [ "br-+" ];
    };

    interfaces = {
      eno1 = {
        ipv4 = {
          addresses = [
            {
              address = "192.168.1.168";
              prefixLength = 24;
            }
          ];

          routes = [
            {
              address = "0.0.0.0";
              prefixLength = 0;
              via = "192.168.1.254";
            }
          ];
        };
      };
    };

    nftables = {
      enable = true;

      ruleset = ''
        table inet excludeTraffic {
          chain allowIncoming {
            type filter hook input priority -100; policy accept;
            tcp dport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedTCPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
            udp dport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedUDPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
          }

          chain allowOutgoing {
            type route hook output priority -100; policy accept;
            tcp sport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedTCPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
            udp sport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedUDPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
          }
        }
      '';
    };
  };
}
