{
  pkgs,
  lib,
  config,
  ...
}:

{
  imports = [
    ./backups.nix
    ./disks.nix
    ./hardware-configuration.nix
    ./logs.nix
    ./networking
    ./services
  ];

  boot = {
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        configurationLimit = 5;
      };
    };

    extraModprobeConfig = "options kvm_intel nested=1";

    binfmt.emulatedSystems = [ "aarch64-linux" ];
  };

  hardware = {
    graphics.enable = true;
  };

  nix.gc.automatic = true;

  networking.hostName = "skarogne";
  networking.hostId = "4a441274";

  time.timeZone = lib.mkDefault "Europe/Paris";

  system.autoUpgrade = {
    enable = true;
    dates = "06:00";
  };

  nixpkgs.config.permittedInsecurePackages = [
    "aspnetcore-runtime-6.0.36"
    "dotnet-sdk-6.0.428"
    "fluffychat-web-1.23.0"
    "olm-3.2.16"
    "openssl-1.1.1w"
  ];

  users.users = {
    ratcornu = {
      extraGroups = [
        "agreg"
        "docker"
        "gollum"
        "libvirtd"
        "medias"
      ];
    };

    pantsushio = {
      isNormalUser = true;
      extraGroups = [ "medias" ];

      home = "/home/pantsushio";
      homeMode = "711";

      hashedPassword = "$y$j9T$OOwSJtSmQBAlBdOrwIKxI0$JFQDdOKQHeW0UiKm1HF6iVpw/Kc7zMXlYn0QJxhO97C";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBiHpmC6RZubwWCg32rYTNHIyfjB34FhmeHVFF8eR1Ie laure@Boeing777-300ER"
      ];
    };

    aeltheos = {
      isNormalUser = true;

      home = "/home/aeltheos";
      homeMode = "711";

      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHc3PC5Cl1AI5jV1glN8TyKZtdaIPKZeF/45ryviRPO8 aeltheos@malygos"
      ];
    };

    polymorphe = {
      isNormalUser = true;

      home = "/home/polymorphe";
      homeMode = "711";

      hashedPassword = "*";
    };

    loutr = {
      isNormalUser = true;
      extraGroups = [ "agreg" ];

      home = "/home/loutr";

      hashedPassword = "*";
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILbUD0bNnZfL01NKKIWgXWQ1Poxu1YPNVhI3oUBGjTUH loutr"
      ];
    };
  };

  users.motdFile = "/etc/motd";

  nixpkgs.config.allowlistedLicenses = with lib.licenses; [ elastic ];

  users.groups = {
    agreg = { };
    medias = { };
  };

  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
    "${config.users.users.ratcornu.home}/.ssh/id_ed25519"
  ];

  environment.systemPackages = with pkgs; [
    kanidm_1_5
    molly-guard
    openldap
  ];

  systemd = {
    services.domainname.enable = false;

    extraConfig = "DefaultLimitNOFILE=65535";
  };

  system.stateVersion = "23.11";

}
