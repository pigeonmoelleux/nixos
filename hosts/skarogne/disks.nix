{ ... }:

{
  services.smartd = {
    enable = true;

    autodetect = true;

    notifications.mail = {
      enable = true;
      sender = "root+smartd@skaven.org";
      recipient = "root@skaven.org";
    };
  };
}
