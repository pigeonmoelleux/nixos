{ pkgs, ... }:

{
  networking = {
    wireguard = {
      enable = true;

      interfaces = {
        tunnels = {
          postSetup = ''
            ${pkgs.iptables}/bin/iptables  -t nat -A POSTROUTING -s 10.13.0.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -A POSTROUTING -s 10.13.9.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -A POSTROUTING -s 10.13.1.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -A POSTROUTING -s 10.13.2.0/24 -d 10.13.0.0/16 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -A POSTROUTING -s 10.13.3.0/24 -d 185.230.78.0/23 -o enp1s0f0 -j MASQUERADE
          '';

          postShutdown = ''
            ${pkgs.iptables}/bin/iptables  -t nat -D POSTROUTING -s 10.13.0.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -D POSTROUTING -s 10.13.9.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -D POSTROUTING -s 10.13.1.0/24 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -D POSTROUTING -s 10.13.2.0/24 -d 10.13.0.0/16 -o enp1s0f0 -j MASQUERADE
            ${pkgs.iptables}/bin/iptables  -t nat -D POSTROUTING -s 10.13.3.0/24 -d 185.230.78.0/23 -o enp1s0f0 -j MASQUERADE
          '';

          peers = [
            {
              # Test
              publicKey = "sZARWY4Q0naTCNJ2iZygeXP/Yl4HdsQFHFdWwu0jmB0=";
              allowedIPs = [ "10.13.2.1/32" ];
            }
            {
              # Téléphone Polymorphe
              publicKey = "o9hm7H8dJ6lodjkzoazcqXtdR7URk40S4yBtT90283o=";
              allowedIPs = [ "10.13.2.2/32" ];
            }
            {
              # Ordi Pantsushio
              publicKey = "juFrEQv3t/MCZ4KqV5eywD84QLg6i9eY0e0gIr/1+ik=";
              allowedIPs = [ "10.13.2.3/32" ];
            }
            {
              # RedDiamond
              publicKey = "XAHej7+y0DqKMyhKCmtvK2KANnaG1MrvIUtEnk94sG0=";
              allowedIPs = [ "10.13.3.1/32" ];
            }
          ];
        };
      };
    };
  };
}
