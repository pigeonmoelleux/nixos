{ lib, config, ... }:

{
  imports = [
    ./fail2ban.nix
    ./netns.nix
    ./vpn.nix
  ];

  networking = {
    domain = "skaven.org";
    extraHosts = ''
      127.0.0.1 auth.skaven.org ikit.skaven.org throt.skaven.org tretch.skaven.org queek.skaven.org technomage.skaven.org vorhax.skaven.org sonarr.skaven.org radarr.skaven.org coureur.skaven.org skrolk.skaven.org prowlarr.skaven.org agreg.info www.agreg.info
    '';
    enableIPv6 = false;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        22
        80
        443
        587
        636
        993
        2049
        4000
        4001
        4002
        7860
        8009
        8080
        8448
        19132
      ];
      allowedUDPPorts = [
        80
        123
        143
        443
        587
        636
        993
        2049
        4000
        4001
        4002
        19004
        19132
        42069
        51818
      ];
      trustedInterfaces = [ "br-+" ];
    };

    nat = {
      enable = true;
      externalInterface = "enp1s0f0";
      internalInterfaces = [ "tunnels" ];
    };

    interfaces = {
      enp1s0f0 = {
        ipv4 = {
          addresses = [
            {
              address = "185.230.78.13";
              prefixLength = 24;
            }
          ];

          routes = [
            {
              address = "0.0.0.0";
              prefixLength = 0;
              via = "185.230.78.99";
            }
          ];
        };
      };
    };

    nftables = {
      enable = true;

      ruleset = ''
        table inet excludeTraffic {
          chain allowIncoming {
            type filter hook input priority -100; policy accept;
            tcp dport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedTCPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
            udp dport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedUDPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
          }

          chain allowOutgoing {
            type route hook output priority -100; policy accept;
            tcp sport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedTCPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
            udp sport { ${
              lib.lists.foldl (
                acc: port: acc + ", " + toString port
              ) "22" config.networking.firewall.allowedUDPPorts
            } } ct mark set 0x00000f41 meta mark set 0x6d6f6c65;
          }
        }
      '';
    };
  };
}
