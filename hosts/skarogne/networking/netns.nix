{ ... }:

{
  networking.vpn-netns = {
    wireguardInterface = "terrier";
    nameserver = "10.2.0.1";

    interfaceNamespace = "netns-terrier";
    vethInterfaceName = "veth-terrier";

    vethIP = "192.168.1.2";
    vethOuterIP = "192.168.1.1";

    wireguardOptions = {
      privateKeyFile = "/etc/wireguard/proton.key";
      ips = [ "10.2.0.2/32" ];

      peers = [
        {
          publicKey = "i0qamQHx9/DRrre/4C+ocY6NVbGSfQBZzRcXYnTEHW0=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "149.88.27.206:51820";
          persistentKeepalive = 25;
        }
      ];
    };

    restrictedServices = [
      "prowlarr"
      # "radarr"
      # "sonarr"
      "suwayomi-server"
      "transmission"
    ];

    portForwarding = {
      enable = true;
    };
  };
}
