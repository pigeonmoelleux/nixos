{ ... }:

{
  services.fail2ban = {
    enable = true;

    ignoreIP = [
      "94.239.68.176"
      "185.230.78.0/23"
    ];
  };
}
