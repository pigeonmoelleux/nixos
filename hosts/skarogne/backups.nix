{ config, ... }:

let
  small_paths = [
    "/etc"
    "/root"
    "/var/data/backups"
    "/var/lib"
  ];

  everything_paths = [
    "/home"
    "/nextcloud"
    "/var/data/mangas"
    "/var/vmail"
  ] ++ small_paths;
in

{
  age.secrets = {
    restic-local-env.file = ../../secrets/skarogne/restic/local-env.age;
    restic-local-pass.file = ../../secrets/skarogne/restic/local-pass.age;
    restic-karak8-repo.file = ../../secrets/skarogne/restic/karak8-repo.age;
    restic-karak8-env.file = ../../secrets/skarogne/restic/karak8-env.age;
    restic-karak8-pass.file = ../../secrets/skarogne/restic/karak8-pass.age;
  };

  services.restic = {
    backups = {
      everything-local = {
        initialize = true;
        repository = "/var/data/restic";
        paths = everything_paths;
        passwordFile = config.age.secrets.restic-local-pass.path;
        environmentFile = config.age.secrets.restic-local-env.path;

        pruneOpts = [
          "--keep-daily 3"
          "--keep-weekly 3"
          "--keep-monthly 3"
          "--keep-yearly 1"
        ];
        timerConfig = {
          OnCalendar = "06:03";
          Persistent = true;
        };
      };

      everything-karak8 = {
        initialize = true;
        repositoryFile = config.age.secrets.restic-karak8-repo.path;
        paths = everything_paths;
        passwordFile = config.age.secrets.restic-karak8-pass.path;
        environmentFile = config.age.secrets.restic-karak8-env.path;

        pruneOpts = [
          "--keep-daily 2"
          "--keep-weekly 2"
          "--keep-monthly 2"
          "--keep-yearly 1"
        ];
        timerConfig = {
          OnCalendar = "05:04";
          Persistent = true;
        };
      };
    };
  };

  services.postgresqlBackup = {
    enable = true;
    location = "/var/data/backups/postgresql";
  };
}
