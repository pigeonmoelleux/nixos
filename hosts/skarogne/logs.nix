{ ... }:

{
  services.journald = {
    forwardToSyslog = true;
  };

  services.rsyslogd = {
    enable = true;

  };
}
