{
  pkgs,
  config,
  lib,
  ...
}:

{
  age.secrets = {
    nextcloud-pass = {
      file = ../../../secrets/skarogne/services/nextcloud-pass.age;
      mode = "440";
      owner = "nextcloud";
      group = "nextcloud";
    };

    nextcloud-db-pass = {
      file = ../../../secrets/skarogne/services/nextcloud-db-pass.age;
      owner = "nextcloud";
      group = "nextcloud";
    };

  };

  services.postgresql = {
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [
      {
        name = "nextcloud";
        ensureDBOwnership = true;
      }
    ];
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud31;

    configureRedis = true;
    hostName = "skrolk.skaven.org";
    https = true;

    maxUploadSize = "16G";

    config = {
      dbtype = "pgsql";
      dbhost = "/run/postgresql";
      dbuser = "nextcloud";
      dbpassFile = config.age.secrets.nextcloud-db-pass.path;

      adminpassFile = config.age.secrets.nextcloud-pass.path;
    };

    phpOptions = {
      "opcache.interned_strings_buffer" = "32";
      "opcache.memory_consumption" = "512";
    };

    appstoreEnable = true;
    extraApps = {
      # calendar = pkgs.fetchzip rec {
      #   name = "calendar";
      #   sha256 = "sha256-8r90o4jtLmTt8FqJCSYseCPse4SQy42iAmyoDhAtlsg=";
      #   url = "https://github.com/nextcloud-releases/calendar/releases/download/v${version}/calendar-v${version}.tar.gz";
      #   version = "4.7.3";
      # };
      # contact = pkgs.fetchzip rec {
      #   name = "contacts";
      #   sha256 = "sha256-EjP5YPBU0dcrsOys+pdjR4scjtA0OUcQqSQS2ct8qQ8=";
      #   url = "https://github.com/nextcloud-releases/contacts/archive/refs/tags/v${version}.zip";
      #   version = "6.0.0";
      # };
      cospend = pkgs.fetchzip rec {
        name = "cospend";
        sha256 = "sha256-PsBgqgUNttGJuCFpX70FMwnFnwqEHcqHJ4G/Q5qHsPM=";
        url = "https://github.com/julien-nc/cospend-nc/releases/download/v${version}/cospend-${version}.tar.gz";
        version = "1.6.1";
      };
      deck = pkgs.fetchzip rec {
        name = "desk";
        sha256 = "sha256-Twhp1YWjy6O3+ZCAfBFNeLK/cm+rxLl5lIrAIXGFH2k=";
        url = "https://github.com/nextcloud-releases/deck/releases/download/v${version}/deck-v${version}.tar.gz";
        version = "1.13.0";
      };
      notes = pkgs.fetchzip rec {
        name = "notes";
        sha256 = "sha256-ptCferIL2GRT3+WNOkahJquefu0v8S0S3bSavuRLLtA=";
        url = "https://github.com/nextcloud-releases/notes/releases/download/v${version}/notes.tar.gz";
        version = "4.10.0";
      };
      tasks = pkgs.fetchzip rec {
        name = "tasks";
        sha256 = "sha256-zMMqtEWiXmhB1C2IeWk8hgP7eacaXLkT7Tgi4NK6PCg=";
        url = "https://github.com/nextcloud/tasks/releases/download/v${version}/tasks.tar.gz";
        version = "0.16.0";
      };
      uppush = pkgs.fetchzip rec {
        name = "uppush";
        sha256 = "sha256-PeKODyAVOfaYlyl07Vcfm4xrhdBcBKyJWi9/50Q4I30=";
        url = "https://codeberg.org/NextPush/uppush/archive/${version}.tar.gz";
        version = "1.4.3";
      };
    };
    extraAppsEnable = true;
  };

  systemd.services = {
    nextcloud-setup = {
      requires = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
    };
  };

  system.activationScripts.nextcloud-symlinks.text =
    let
      users = [ "ratcornu" ];
    in
    lib.strings.concatMapStrings (user: ''
      if [[ ! -h "/home/${user}/nextcloud" ]]; then
        ln -s "/nextcloud/${user}" "/home/${user}/nextcloud"
      fi
    '') users;

  fileSystems = {
    "/nextcloud/ratcornu/Musiques" = {
      device = "/home/ratcornu/musiques";
      options = [ "bind" ];
    };
  };

  services.nginx.virtualHosts = {
    "skrolk.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/_matrix/push/v1/notify" = {
        proxyPass = "https://skrolk.skaven.org/index.php/apps/uppush/gateway/matrix";
      };

      extraConfig = ''
        proxy_connect_timeout   10m;
        proxy_send_timeout      10m;
        proxy_read_timeout      10m;
        proxy_buffering off;
      '';
    };

    "nextcloud.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "skrolk.skaven.org";
    };
  };
}
