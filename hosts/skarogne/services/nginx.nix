{ pkgs, ... }:

{
  security.acme = {
    acceptTerms = true;
    defaults.email = "balthazar.patiachvili@crans.org";
  };

  users.users.nginx.extraGroups = [ "medias" ];
  services.nginx = {
    enable = true;

    additionalModules = with pkgs.nginxModules; [
      geoip2
    ];

    recommendedProxySettings = true;
    recommendedOptimisation = true;
    clientMaxBodySize = "16G";

    virtualHosts = {
      "skaven.org" = {
        enableACME = true;
        forceSSL = true;

        root = "/var/www/skaven.org/";

        locations = {
          "/" = {
            index = "index.html index.htm index.php";
          };
        };
      };

      "test.skaven.org" = {
        forceSSL = true;
        enableACME = true;
      };

      "agreg.info" = {
        enableACME = true;
        forceSSL = true;

        root = "/var/www/agreg.info/";

        locations."/" = {
          index = "index.html index.htm";
        };
      };

      "www.agreg.info" = {
        enableACME = true;
        forceSSL = true;

        globalRedirect = "agreg.info";
      };

      "patiachvili.fr" = {
        enableACME = true;
        forceSSL = true;

        root = "/var/www/patiachvili.fr/";

        locations."/" = {
          index = "index.html index.htm";
        };
      };

      "www.patiachvili.fr" = {
        enableACME = true;
        forceSSL = true;

        globalRedirect = "patiachvili.fr";
      };
    };
  };

  users.users.oiseau = {
    isNormalUser = true;
    home = "/home/oiseau";

    hashedPassword = "*";
    openssh.authorizedKeys.keys = [
      " ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJsubUi4p7hk/vFmY23wyIHJU1PQG7064CbSP8zm7ZfM oiseau@skarogne"
    ];
  };
}
