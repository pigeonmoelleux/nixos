{ pkgs, config, ... }:

{
  age.secrets = {
    spyware-env-file = {
      file = ../../../secrets/skarogne/services/spyware-env-file.age;
      owner = "ratcornu";
      group = "users";
    };
  };

  systemd.services = {
    "spyware" = {
      enable = true;

      requires = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      unitConfig = {
        Description = "Bot discord pour la Foire aux Monstres";
      };

      serviceConfig = {
        Type = "simple";
        User = "ratcornu";
        Group = "users";

        Restart = "on-failure";
        RestartSec = 10;
        EnvironmentFile = config.age.secrets.spyware-env-file.path;

        PermissionsStartOnly = true;
        SyslogIdentifier = "spyware";

        ExecStart = "${pkgs.spyware}/bin/spyware /var/data/spyware";
      };
    };
  };
}
