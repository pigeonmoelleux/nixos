{ pkgs, lib, ... }:

{
  services.postgresql = {
    enable = true;
    package = lib.mkDefault pkgs.postgresql_14;
  };
}
