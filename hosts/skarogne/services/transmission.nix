{
  pkgs,
  config,
  lib,
  ...
}:

{

  users.users.transmission.extraGroups = [ "medias" ];
  services.transmission = {
    enable = true;
    package = pkgs.transmission_4;

    group = "medias";

    openFirewall = true;
    openRPCPort = false;

    settings = {
      port-forwarding-enable = true;

      download-dir = "/var/data/transmission/downloads";
      incomplete-dir = "/var/data/transmission/incomplete";

      rpc-port = 9091;
      rpc-bind-address = "0.0.0.0";

      rpc-whitelist-enabled = false;
      rpc-host-whitelist-enabled = false;
      rpc-authentication-required = false;

      ratio-limit-enabled = false;
      idle-seeding-limit-enabled = false;

      speed-limit-down-enabled = true;
      speed-limit-down = 20000;
      speed-limit-up-enabled = true;
      speed-limit-up = 50;

      peer-limit-global = 100;
      peer-limit-per-torrent = 20;
    };
  };

  networking.vpn-netns.encapsulatedServices.transmission = {
    enable = true;

    portForwarding =
      let
        settingsFile = "/var/lib/transmission/.config/transmission-daemon/settings.json";
        jq = lib.getExe pkgs.jq;
      in
      {
        enable = true;
        updateScript = ''
          CURRENT_PORT=$(${jq} '."peer-port"' ${settingsFile})
          test "$PORT" -eq "$CURRENT_PORT" || (
            TMP=$(mktemp);
            ${jq} ".\"peer-port\" = $PORT" ${settingsFile} >"$TMP";
            install -D -m 600 -o '${config.services.transmission.user}' -g '${config.services.transmission.group}' "$TMP" ${settingsFile};
            rm "$TMP";
            systemctl reload transmission
            echo "updated transmission configuration";
          )
        '';
      };
  };

  systemd.services.transmission = {
    environment = {
      TRANSMISSION_WEB_HOME = lib.mkForce (
        pkgs.fetchzip {
          url = "https://github.com/johman10/flood-for-transmission/releases/download/latest/flood-for-transmission.zip";
          sha256 = "sha256-arC73X+IqXjU2ccV35IlUW9S7GCI5GGD7fJ50JTbqmk=";
        }
      );
    };
  };

  services.nginx.virtualHosts = {
    "coureur.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "https://localhost:9443/";
      };
    };
  };
}
