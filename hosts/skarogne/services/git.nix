{ config, pkgs, ... }:

{
  age.secrets = {
    git-codeberg-token.file = ../../../secrets/skarogne/services/git-codeberg-token.age;
    git-poulet-token.file = ../../../secrets/skarogne/services/git-poulet-token.age;
  };

  services.gitea-actions-runner = {
    instances = {
      codeberg = {
        enable = true;
        name = "codeberg";
        url = "https://codeberg.org";
        tokenFile = config.age.secrets.git-codeberg-token.path;
        labels = [
          "debian-latest:docker://debian:latest"
          "ubuntu-latest:docker://gitea/runner-images:ubuntu-latest"
        ];
        hostPackages = with pkgs; [
          bash
          coreutils
          curl
          gawk
          git
          gnused
          wget
        ];
      };
      poulet = {
        enable = true;
        name = "poulet";
        url = "https://codeberg.org";
        tokenFile = config.age.secrets.git-poulet-token.path;
        labels = [
          "debian-latest:docker://debian:latest"
          "ubuntu-latest:docker://gitea/runner-images:ubuntu-latest"
        ];
        hostPackages = with pkgs; [
          bash
          coreutils
          curl
          gawk
          git
          gnused
          wget
        ];
      };
    };
  };
}
