{ ... }:

{
  imports = [
    ./arr.nix
    ./auth.nix
    ./dokuwiki.nix
    ./git.nix
    ./gollum.nix
    ./grafana.nix
    ./jellyfin.nix
    ./jellyseerr.nix
    ./komga.nix
    ./mails.nix
    ./matrix.nix
    ./minecraft.nix
    ./nextcloud.nix
    ./nginx.nix
    ./pingvin.nix
    ./postgresql.nix
    ./rss.nix
    ./spyware.nix
    ./suwayomi.nix
    ./syncthing.nix
    # ./syncyomi.nix
    ./transmission.nix
    ./vega.nix
    ./wiki-js.nix
  ];

  users.groups.ldap-reader = { };
  age.secrets = {
    ldap-admin-pass = {
      file = ../../../secrets/skarogne/services/ldap-admin-pass.age;
      group = "ldap-reader";
      mode = "440";
    };
  };
}
