{ config, lib, ... }:

{
  mailserver = {
    enable = true;

    fqdn = "ikit.skaven.org";
    domains = [
      "skaven.org"
      "agreg.info"
    ];

    certificateScheme = "manual";
    certificateFile = "/var/lib/acme/${config.mailserver.fqdn}/fullchain.pem";
    keyFile = "/var/lib/acme/${config.mailserver.fqdn}/key.pem";

    localDnsResolver = false;
    enableManageSieve = true;

    ldap = {
      enable = true;

      uris = [ "ldap://" ];
      searchBase = "dc=throt,dc=skaven,dc=org";
      searchScope = "sub";

      bind = {
        dn = "cn=admin,dc=throt,dc=skaven,dc=org";
        passwordFile = config.age.secrets.ldap-admin-pass.path;
      };

      dovecot = {
        userFilter = "(&(objectClass=inetLocalMailRecipient)(mailRoutingAddress=%u))";
        passFilter = "(&(objectClass=inetLocalMailRecipient)(mailRoutingAddress=%u))";
      };

      postfix = {
        filter = "(&(objectClass=inetLocalMailRecipient)(|(mailLocalAddress=%s)(mailRoutingAddress=%s)))";
        mailAttribute = "mailRoutingAddress";
        uidAttribute = "mailRoutingAddress";
      };
    };
  };

  services.dovecot2 = {
    extraConfig = ''
      plugin {
        sieve_extensions = +editheader
        sieve_max_redirects = 25
      }
    '';
  };

  services.postfix = {
    config = {
      "local_recipient_maps" = "";
      "virtual_alias_maps" = lib.mkForce "ldap:/run/postfix/ldap-virtual-mailbox-map.cf";
    };
  };

  services.roundcube = {
    enable = true;

    hostName = "ikit.skaven.org";
    extraConfig = ''
      $config['smtp_host'] = "tls://ikit.skaven.org";
      $config['smtp_port'] = 587;
      $config['smtp_user'] = "%u";
      $config['smtp_pass'] = "%p";
    '';
  };

  systemd.services.authentik = {
    requires = [ "network-online.target" ];
  };

  services.nginx.virtualHosts = {
    "ikit.skaven.org" = {
      enableACME = true;
      forceSSL = true;
    };

    "roundcube.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "ikit.skaven.org";
    };
  };
}
