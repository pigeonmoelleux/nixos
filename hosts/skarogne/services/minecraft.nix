{
  pkgs,
  config,
  lib,
  ...
}:

{
  nixpkgs.config.allowUnfreePredicate =
    pkg:
    builtins.elem (lib.getName pkg) [
      "minecraft-server"
    ];

  services.minecraft-server = {
    enable = true;
    package =
      let
        version = "1.21.4";
        url = "https://piston-data.mojang.com/v1/objects/4707d00eb834b446575d89a61a11b5d548d8c001/server.jar";
        sha256 = "sha256-EGaXCwnpxnGERXIpHEqHHMGsK4WDi/cAT6DneOEPE1g=";
      in
      (pkgs.minecraftServers.vanilla-1-20.overrideAttrs (old: rec {
        name = "minecraft-server-${version}";
        inherit version;
        src = pkgs.fetchurl {
          inherit url sha256;
        };
      }));

    eula = true;
    openFirewall = true;
    declarative = true;

    jvmOpts = "-Xmx16192M -Xms16192M";

    serverProperties = {
      server-port = 25565;
      difficulty = "easy";
      gamemode = "survival";
      max-players = 10;
      motd = "L'entrée de Skarogne";
      enable-rcon = false;
    };
  };
}
