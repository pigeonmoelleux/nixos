{
  config,
  pkgs,
  lib,
  ...
}:

let
  hostname = "throt.skaven.org";
  dbSuffix = "dc=throt,dc=skaven,dc=org";
  initialContent = {
    "${dbSuffix}" = ''
      dn: ${dbSuffix}
      objectClass: top
      objectClass: dcObject
      objectClass: organization
      o: throt.skaven.org

      dn: ou=users,${dbSuffix}
      objectClass: organizationalUnit
      objectClass: top
      ou: users

      dn: ou=groups,${dbSuffix}
      objectClass: organizationalUnit
      objectClass: top
      ou: groups

      dn: ou=system-users,${dbSuffix}
      objectClass: organizationalUnit
      objectClass: top
      ou: system-users
    '';
  };
in

{
  users.groups.nginx.members = [ "openldap" ];

  systemd.services.openldap = {
    wants = [ "acme-${hostname}.service" ];
    after = [ "acme-${hostname}.service" ];
  };

  services.openldap = {
    enable = true;

    urlList = [
      "ldap:///"
      "ldaps:///"
    ];
    mutableConfig = false;

    settings = {
      attrs = {
        olcLogLevel = "conns config";
        olcTLSCACertificateFile = "/var/lib/acme/${hostname}/full.pem";
        olcTLSCertificateFile = "/var/lib/acme/${hostname}/cert.pem";
        olcTLSCertificateKeyFile = "/var/lib/acme/${hostname}/key.pem";
        olcTLSCipherSuite = "HIGH:MEDIUM:+3DES:+RC4:+aNULL";
        olcTLSCRLCheck = "none";
        olcTLSVerifyClient = "never";
        olcTLSProtocolMin = "3.1";
      };

      children = {
        "cn=schema".includes = [
          "${pkgs.openldap}/etc/schema/core.ldif"
          "${pkgs.openldap}/etc/schema/cosine.ldif"
          "${pkgs.openldap}/etc/schema/inetorgperson.ldif"
          "${pkgs.openldap}/etc/schema/misc.ldif"
          "${pkgs.openldap}/etc/schema/nis.ldif"
        ];

        "olcDatabase={1}mdb" = {
          attrs = {
            objectClass = [
              "olcDatabaseConfig"
              "olcMdbConfig"
            ];

            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/lib/openldap/data";

            olcSuffix = "${dbSuffix}";

            olcRootDN = "cn=admin,${dbSuffix}";
            olcRootPW = "{SSHA}XNdVm3XCAKHx+w7AMw4wah+fG+OBs5oh";

            olcAccess = [
              ''
                {1}to *
                  by self write
                  by anonymous auth
                  by * none
              ''

              ''
                {0}to attrs=userPassword
                  by self write
                  by anonymous auth
                  by * none
              ''
            ];
          };

          children = {
            "olcOverlay={2}ppolicy".attrs = {
              objectClass = [
                "olcOverlayConfig"
                "olcPPolicyConfig"
                "top"
              ];
              olcOverlay = "{2}ppolicy";
              olcPPolicyHashCleartext = "TRUE";
            };

            "olcOverlay={3}memberof".attrs = {
              objectClass = [
                "olcOverlayConfig"
                "olcMemberOf"
                "top"
              ];
              olcOverlay = "{3}memberof";
              olcMemberOfRefInt = "TRUE";
              olcMemberOfDangling = "ignore";
              olcMemberOfGroupOC = "groupOfNames";
              olcMemberOfMemberAD = "member";
              olcMemberOfMemberOfAD = "memberOf";
            };

            "olcOverlay={4}refint".attrs = {
              objectClass = [
                "olcOverlayConfig"
                "olcRefintConfig"
                "top"
              ];
              olcOverlay = "{4}refint";
              olcRefintAttribute = "memberof member manager owner";
            };
          };
        };
      };
    };

    # declarativeContents = initialContent;
  };

  age.secrets = {
    authentik-env-file.file = ../../../secrets/skarogne/services/authentik-env-file.age;
    kanidm-admin-password = {
      owner = "kanidm";
      file = ../../../secrets/skarogne/services/kanidm-admin-password.age;
    };
    kanidm-idm-admin-password = {
      owner = "kanidm";
      file = ../../../secrets/skarogne/services/kanidm-idm-admin-password.age;
    };
  };

  services.authentik = {
    enable = true;

    environmentFile = "${config.age.secrets.authentik-env-file.path}";
    settings = {
      email = {
        host = "ikit.skaven.org";
        port = 587;
        username = "throt@skaven.org";
        use_tls = true;
        use_ssl = false;
        from = "throt@skaven.org";
      };
      disable_startup_analytics = true;
      avatars = "initials";
    };

    nginx = {
      enable = true;

      enableACME = true;
      host = "throt.skaven.org";
    };
  };

  users.users.kanidm.extraGroups = [ "nginx" ];
  services.kanidm =
    let
      hostname = "auth.skaven.org";
    in
    {
      enableServer = true;
      package = pkgs.kanidmWithSecretProvisioning_1_5;

      serverSettings = {
        bindaddress = "127.0.0.1:10443";
        domain = hostname;
        ldapbindaddress = "127.0.0.1:10636";
        origin = "https://${hostname}";
        tls_chain = "/var/lib/acme/${hostname}/cert.pem";
        tls_key = "/var/lib/acme/${hostname}/key.pem";

        online_backup = {
          path = "/var/data/backups/kanidm";
          schedule = "00 06 * * *";
          versions = 7;
        };
      };

      provision = {
        enable = true;
        adminPasswordFile = config.age.secrets.kanidm-admin-password.path;
        idmAdminPasswordFile = config.age.secrets.kanidm-idm-admin-password.path;
      };
    };

  systemd.services."kanidm" = {
    serviceConfig = {
      BindReadOnlyPaths = lib.mkForce [
        "/nix/store"
        "/run/systemd/notify"
        "-/etc/resolv.conf"
        "-/etc/nsswitch.conf"
        "-/etc/hosts"
        "-/etc/localtime"
        "/var/lib/acme/auth.skaven.org"
        "/run/agenix"
      ];
      BindPaths = [
        "/var/lib/kanidm"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "auth.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "https://${config.services.kanidm.serverSettings.bindaddress}";
      };
    };

    "services.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "throt.skaven.org";
    };
  };
}
