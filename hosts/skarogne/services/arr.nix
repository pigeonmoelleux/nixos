{ lib, ... }:

{

  age.secrets = {
    sonarr-token = {
      file = ../../../secrets/skarogne/services/sonarr-token.age;
      group = "medias";
      mode = "440";
    };
    radarr-token = {
      file = ../../../secrets/skarogne/services/radarr-token.age;
      group = "medias";
      mode = "440";
    };
    prowlarr-token = {
      file = ../../../secrets/skarogne/services/prowlarr-token.age;
      group = "medias";
      mode = "440";
    };
  };

  users.users.sonarr.extraGroups = [ "medias" ];
  services.sonarr = {
    enable = true;

    openFirewall = false;
    group = "medias";
  };

  users.users.radarr.extraGroups = [ "medias" ];
  services.radarr = {
    enable = true;

    openFirewall = false;
    group = "medias";
  };

  users.users.prowlarr = {
    isSystemUser = true;
    group = "medias";
  };
  services.prowlarr = {
    enable = true;

    openFirewall = false;
  };

  systemd.services.prowlarr = {
    serviceConfig = {
      DynamicUser = lib.mkForce false;
      User = "prowlarr";
      Group = "medias";
    };
  };

  services.nginx.virtualHosts = {
    "radarr.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "https://localhost:9443/";
      };
    };

    "sonarr.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "https://localhost:9443/";
      };
    };

    "prowlarr.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "https://localhost:9443/";
      };
    };
  };
}
