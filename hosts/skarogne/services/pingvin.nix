{ pkgs, ... }:

{
  services.pingvin-share = {
    enable = true;

    hostname = "snikch.skaven.org";
    https = true;

    backend = {
      port = 9010;
    };

    frontend = {
      port = 9011;
    };

    nginx.enable = true;
  };

  services.nginx.virtualHosts = {
    "pingvin.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      globalRedirect = "snikch.skaven.org";
    };
  };
}
