{ config, ... }:

{
  services.gollum = {
    enable = true;

    port = 4978;
    branch = "main";

    emoji = true;
    h1-title = true;
    math = true;
    no-edit = true;
  };

  services.nginx.virtualHosts = {
    "tretch.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        # proxyPass = "http://localhost:${toString config.services.gollum.port}";
        proxyPass = "https://skaven.org/";
      };
    };

    "documentation.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "tretch.skaven.org";
    };
  };
}
