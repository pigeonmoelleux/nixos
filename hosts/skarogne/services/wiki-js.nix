{ pkgs, config, ... }:

{
  services.postgresql = {
    enable = true;

    authentication = pkgs.lib.mkOverride 10 ''
      #type database  DBuser  auth-method
      local all       all     trust
    '';

    ensureUsers = [
      {
        name = "wiki";
        ensureDBOwnership = true;
      }
    ];

    ensureDatabases = [
      "wiki"
    ];
  };

  services.wiki-js = {
    enable = true;

    stateDirectoryName = "wiki-kaj";

    settings = {
      port = 14506;
      bindIP = "127.0.0.1";

      db = {
        db = "wiki";
        user = "wiki";
        host = "/run/postgresql";
      };
    };
  };

  fileSystems = {
    "/var/lib/wiki-kaj/backups" = {
      device = "/var/backups/wiki-kaj";
      options = [ "bind" ];
    };
  };

  services.nginx.virtualHosts."wiki.kaj.skaven.org" = {
    forceSSL = true;
    enableACME = true;

    locations."/" = {
      proxyPass = "http://localhost:${toString config.services.wiki-js.settings.port}";
    };
  };
}
