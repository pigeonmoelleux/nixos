{ config, ... }:

{
  services.komga = {
    enable = true;

    group = "medias";

    settings = {
      port = "9081";
    };
  };

  services.nginx.virtualHosts = {
    "skritch.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.komga.settings.port}/";
      };
    };

    "komga.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "skritch.skaven.org";
    };
  };
}
