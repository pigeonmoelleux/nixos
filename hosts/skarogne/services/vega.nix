{ ... }:

{
  services.nginx.virtualHosts = {
    "pantsushio.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://localhost:36987";
      };
    };
  };
}
