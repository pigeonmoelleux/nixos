{
  pkgs,
  config,
  lib,
  ...
}:

{
  services.nginx.virtualHosts = {
    "moine.skaven.org" = {
      forceSSL = true;
      enableACME = true;
    };

    "rss.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "moine.skaven.org";
    };
  };
}
