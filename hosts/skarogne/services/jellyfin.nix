{ pkgs, ... }:

let
  webUIPort = 8096;
in

{
  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
      vaapiVdpau
      libvdpau-va-gl
      intel-compute-runtime
    ];
  };

  users.users.jellyfin.extraGroups = [ "medias" ];
  services.jellyfin = {
    enable = true;

    group = "medias";
  };

  services.nginx.virtualHosts = {
    "thanquol.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://localhost:${toString webUIPort}";
      };

      locations."/metrics" = {
        proxyPass = "http://localhost:${toString webUIPort}";
        extraConfig = ''
          allow 127.0.0.1;
          allow ::1;
          allow 185.230.78.13;
          allow 10.13.0.0/24;
          allow 10.13.9.0/24;
          allow 192.168.1.0/30;
          deny  all;
        '';
      };
    };

    "jellyfin.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      globalRedirect = "thanquol.skaven.org";
    };
  };
}
