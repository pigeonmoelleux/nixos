{ pkgs, config, ... }:

{
  services.jellyseerr = {
    enable = true;

    port = 5055;
    openFirewall = true;
  };

  services.nginx.virtualHosts = {
    "vorhax.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/robots.txt" = {
        extraConfig = ''
          add_header Content-Type text/plain;
          return 200 "User-agent: *\nDisallow: /\n";
        '';
      };

      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.jellyseerr.port}/";
      };
    };

    "jellyseerr.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "vorhax.skaven.org";
    };
  };
}
