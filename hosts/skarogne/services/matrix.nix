{
  pkgs,
  config,
  lib,
  ...
}:

let
  port = 8008;
in

{
  age.secrets = {
    matrix-coturn-secret = {
      file = ../../../secrets/skarogne/services/matrix-coturn-secret.age;
      owner = "turnserver";
    };

    matrix-extra-config = {
      file = ../../../secrets/skarogne/services/matrix-extra-config.age;
      owner = "matrix-synapse";
    };

    lk-jwt-service-env = {
      file = ../../../secrets/skarogne/services/lk-jwt-service-env.age;
      owner = "matrix-synapse";
    };
  };

  services.matrix-synapse = {
    enable = true;

    plugins = with config.services.matrix-synapse.package.plugins; [
      matrix-synapse-ldap3
    ];

    settings = {
      server_name = "skaven.org";

      database = {
        name = "psycopg2";
        args = {
          user = "matrix-synapse";
          database = "matrix-synapse";
        };
      };

      listeners = [
        {
          port = port;
          bind_addresses = [ "127.0.0.1" ];
          type = "http";
          tls = false;
          x_forwarded = true;
          resources = [
            {
              names = [
                "client"
                "federation"
              ];
              compress = true;
            }
          ];
        }
      ];

      rc_message = {
        per_second = 0.5;
        burst_count = 30;
      };

      turn_uris = [
        "turn:${config.services.coturn.realm}:3478?transport=udp"
        "turn:${config.services.coturn.realm}:3478?transport=tcp"
      ];
      turn_user_lifetime = "1h";

      modules = [
        {
          module = "ldap_auth_provider.LdapAuthProviderModule";
          config = {
            enabled = true;
            uri = "ldap://throt.skaven.org:389";
            start_tls = false;
            base = "ou=users,dc=throt,dc=skaven,dc=org";
            attributes = {
              uid = "cn";
              mail = "mail";
              name = "sn";
            };
            binddn = "cn=admin,dc=throt,dc=skaven,dc=org";
            bind_password_file = config.age.secrets.ldap-admin-pass.path;
            filter = "(objectClass=inetOrgPerson)";
          };
        }
      ];

      experimental_features = {
        msc2409_to_device_messages_enabled = true;
        msc3202_device_masquerading = true;
        msc3202_transaction_extensions = true;
        msc3266_enabled = true;
        msc4222_enabled = true;
      };
    };

    extraConfigFiles = [ config.age.secrets.matrix-extra-config.path ];
  };

  services.coturn = rec {
    enable = true;
    no-cli = true;
    no-tcp-relay = true;
    min-port = 49000;
    max-port = 50000;
    use-auth-secret = true;
    static-auth-secret-file = config.age.secrets.matrix-coturn-secret.path;
    realm = "skaven.org";
    cert = "${config.security.acme.certs.${realm}.directory}/full.pem";
    pkey = "${config.security.acme.certs.${realm}.directory}/key.pem";
    extraConfig = ''
      verbose
      no-multicast-peers
      denied-peer-ip=0.0.0.0-0.255.255.255
      denied-peer-ip=10.0.0.0-10.255.255.255
      denied-peer-ip=100.64.0.0-100.127.255.255
      denied-peer-ip=127.0.0.0-127.255.255.255
      denied-peer-ip=169.254.0.0-169.254.255.255
      denied-peer-ip=172.16.0.0-172.31.255.255
      denied-peer-ip=192.0.0.0-192.0.0.255
      denied-peer-ip=192.0.2.0-192.0.2.255
      denied-peer-ip=192.88.99.0-192.88.99.255
      denied-peer-ip=192.168.0.0-192.168.255.255
      denied-peer-ip=198.18.0.0-198.19.255.255
      denied-peer-ip=198.51.100.0-198.51.100.255
      denied-peer-ip=203.0.113.0-203.0.113.255
      denied-peer-ip=240.0.0.0-255.255.255.255
      denied-peer-ip=::1
      denied-peer-ip=64:ff9b::-64:ff9b::ffff:ffff
      denied-peer-ip=::ffff:0.0.0.0-::ffff:255.255.255.255
      denied-peer-ip=100::-100::ffff:ffff:ffff:ffff
      denied-peer-ip=2001::-2001:1ff:ffff:ffff:ffff:ffff:ffff:ffff
      denied-peer-ip=2002::-2002:ffff:ffff:ffff:ffff:ffff:ffff:ffff
      denied-peer-ip=fc00::-fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
      denied-peer-ip=fe80::-febf:ffff:ffff:ffff:ffff:ffff:ffff:ffff
    '';
  };

  systemd.services."lk-jwt-service" = {
    enable = true;

    requires = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    before = [ "matrix-synapse.service" ];

    unitConfig = {
      Description = "LiveKit Management Service for Element-Call";
    };

    serviceConfig = {
      Type = "simple";
      User = "matrix-synapse";
      Group = "matrix-synapse";

      Restart = "on-failure";
      RestartSec = 10;
      EnvironmentFile = config.age.secrets.lk-jwt-service-env.path;

      PermissionsStartOnly = true;
      SyslogIdentifier = "matrix-lk-jwt-service";

      ExecStart = "${lib.getExe pkgs.lk-jwt-service}";
    };
  };

  networking.firewall = {
    allowedTCPPorts = [
      3478
      5349
    ];
    allowedUDPPorts = [
      3478
      5349
    ];
    allowedUDPPortRanges = [
      {
        from = config.services.coturn.min-port;
        to = config.services.coturn.max-port;
      }
    ];
  };

  services.nginx.virtualHosts = {
    "skaven.org" =
      let
        clientConfig = {
          "m.homeserver".base_url = "https://skaven.org";
          "org.matrix.msc4143.rtc_foci" = [
            {
              "type" = "livekit";
              "livekit_service_url" = "https://livekit.skaven.org";
            }
          ];
        };
        serverConfig."m.server" = "skaven.org:443";
        mkWellKnown = data: ''
          add_header Content-Type application/json;
          add_header Access-Control-Allow-Origin *;
          return 200 '${builtins.toJSON data}';
        '';
      in
      {
        forceSSL = true;
        enableACME = true;

        listen = [
          {
            addr = "0.0.0.0";
            port = 80;
            ssl = false;
          }
          {
            addr = "0.0.0.0";
            port = 443;
            ssl = true;
          }
          {
            addr = "0.0.0.0";
            port = 8448;
            ssl = true;
          }
        ];

        locations."/_matrix" = {
          proxyPass = "http://localhost:${toString port}";
          extraConfig = ''
            proxy_set_header X-Forwarded-For $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $host;
          '';
        };

        locations."/_synapse/client" = {
          proxyPass = "http://localhost:${toString port}";
          extraConfig = ''
            proxy_set_header X-Forwarded-For $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $host;
          '';
        };

        locations."= /.well-known/matrix/server".extraConfig = mkWellKnown serverConfig;
        locations."= /.well-known/matrix/client".extraConfig = mkWellKnown clientConfig;
      };

    "skweel.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      root = pkgs.fluffychat-web;
    };

    "matrix.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "skweel.skaven.org";
    };

    "element.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      root = pkgs.element-web.override {
        conf = {
          default_server_config = {
            "m.homeserver" = {
              base_url = "https://skaven.org";
              server_name = "skaven.org";
            };
          };
          default_theme = "dark";
          features = {
            feature_video_rooms = true;
            feature_group_calls = true;
            feature_element_call_video_rooms = true;
          };
        };
      };
    };

    "fluffy.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      globalRedirect = "skweel.skaven.org";
    };

    "call.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      root =
        let
          format = pkgs.formats.json { };
          element-call-config = {
            "default_server_config" = {
              "m.homeserver" = {
                "base_url" = "http://localhost:8008";
                "server_name" = "skaven.org";
              };
            };
            "livekit" = {
              "livekit_service_url" = "http://localhost:8808";
            };
            "features" = {
              "feature_use_device_session_member_events" = true;
            };
            "eula" = "https://static.element.io/legal/online-EULA.pdf";
          };
          element-call-config-file = format.generate "config.json" element-call-config;
        in
        pkgs.element-call.overrideAttrs (old: {
          installPhase = ''
            runHook preInstall
            mkdir $out
            cp -r dist/* $out
            runHook postInstall
          '';

          postInstall = ''
            cp ${element-call-config-file} $out/config.json
          '';
        });

      locations."/" = {
        tryFiles = "$uri /$uri /index.html";
      };
    };

    "livekit.skaven.org" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://localhost:8808";
        extraConfig = ''
          add_header 'Access-Control-Allow-Origin' '*';
        '';
      };
    };
  };
}
