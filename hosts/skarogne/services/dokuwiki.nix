{ config, ... }:

{
  users.users.dokuwiki = {
    extraGroups = [ "ldap-reader" ];
  };

  services.dokuwiki = {
    sites."histoire.skaven.org" = {
      enable = true;

      pluginsConfig = {
        authldap = true;
        mathajx = true;
      };

      settings = {
        title = "De l'Histoire";
        start = "Nœud";
        lang = "fr";

        useacl = true;
        authtype = "authldap";
        passcrypt = "sha512";
        superuser = "ratcornu";
        disableactions = [ "register" ];

        plugin = {
          authldap = {
            server = "ldap://throt.skaven.org:389";
            version = 3;
            starttls = 0;

            binddn = "cn=admin,dc=throt,dc=skaven,dc=org";
            bindpw._file = config.age.secrets.ldap-admin-pass.path;

            usertree = "ou=users,dc=throt,dc=skaven,dc=org";
            grouptree = "ou=groups,dc=throt,dc=skaven,dc=org";
            userfilter = "(&(objectClass=inetOrgPerson)(cn=%{user})";
            groupfilter = "(&(objectClass=groupOfNames)(member=%{dn}))";
            attributes._raw = "array('cn', 'sn', 'mail', 'memberof')";
            userscope = "sub";
            groupscope = "sub";
            userkey = "cn";
            groupkey = "cn";

            debug = 1;
          };
        };
      };

      acl = [
        {
          page = "*";
          actor = "@external";
          level = "none";
        }
        {
          page = "*";
          actor = "@histoire";
          level = "read";
        }
        {
          page = "*";
          actor = "ratcornu";
          level = "delete";
        }
      ];
    };
  };

  services.nginx.virtualHosts = {
    "histoire.skaven.org" = {
      enableACME = true;
      forceSSL = true;
    };

    "wiki.skaven.org" = {
      enableACME = true;
      forceSSL = true;
    };
  };
}
