{
  pkgs,
  config,
  lib,
  ...
}:

let
  ldap_config = ''
    [[servers]]
    host = "throt.skaven.org"
    port = 389
    use_ssl = false
    start_tls = false
    tls_ciphers = []
    ssl_skip_verify = true

    bind_dn = "cn=admin,dc=throt,dc=skaven,dc=org"
    bind_password = """$__file{${config.age.secrets.ldap-admin-pass.path}}"""

    search_filter = "(cn=%s)"
    search_base_dns = [ "ou=users,dc=throt,dc=skaven,dc=org" ]

    [servers.attributes]
    username = "cn"
    member_of = "memberOf"
    email = "email"


    [[servers.group_mappings]]
    group_dn = "cn=admins,ou=groups,dc=throt,dc=skaven,dc=org"
    org_role = "Admin"

    [[servers.group_mappings]]
    group_dn = "*"
    org_role = "Viewer"
  '';
  ldap_config_file = pkgs.writeText "ldap.toml" ldap_config;
in

{
  services.grafana = {
    enable = true;

    settings = {
      server = {
        http_addr = "127.0.0.1";
        http_port = 2342;
        domain = "technomage.skaven.org";
        root_url = "https://technomage.skaven.org/";
      };

      "auth.ldap" = {
        enabled = true;
        config_file = "${ldap_config_file}";
        allow_sign_up = true;
      };

      log = {
        filters = "ldap:debug";
      };
    };
  };

  systemd.services.grafana = {
    serviceConfig = {
      Group = "ldap-reader";
    };
  };

  services.prometheus = {
    enable = true;

    port = 2343;

    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        port = 10001;
      };
      systemd.enable = true;
      nginx.enable = true;
      nextcloud = {
        enable = true;
        url = "https://skrolk.skaven.org/ocs/v2.php/apps/serverinfo/api/v1/info";
        group = "nextcloud";
        username = "root";
        passwordFile = config.age.secrets.nextcloud-pass.path;
      };
    };

    scrapeConfigs = [
      {
        job_name = "technomage";
        static_configs = [
          {
            targets =
              let
                exporters = lib.filterAttrs (
                  n: v: n != "assertions" && n != "warnings" && n != "json" && n != "minio" && n != "tor" && v.enable
                ) config.services.prometheus.exporters;
              in
              lib.mapAttrsToList (_: attrs: "localhost:${toString attrs.port}") exporters;
          }
        ];
      }
    ];
  };

  services.nginx.virtualHosts = {

    "technomage.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.grafana.settings.server.http_port}/";
        proxyWebsockets = true;
      };
    };

    "grafana.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      globalRedirect = "technomage.skaven.org";
    };
  };
}
