{
  pkgs,
  config,
  ...
}:

{
  services.suwayomi-server = {
    enable = true;
    package = pkgs.ratcornu.suwayomi-server;

    group = "medias";
    openFirewall = true;

    settings = {
      server = {
        ip = config.networking.vpn-netns.vethIP;
        port = 4568;

        extensionRepos = [
          "https://raw.githubusercontent.com/keiyoushi/extensions/repo/index.min.json"
        ];

        downloadAsCbz = true;

        basicAuthEnabled = false;

        backupPath = "/var/data/backups/suwayomi";
        backupTime = "07:11";
      };
    };
  };

  fileSystems = {
    "/var/lib/suwayomi-server/.local/share/Tachidesk/downloads/mangas" = {
      device = "/var/data/mangas";
      options = [ "bind" ];
    };
  };

  services.nginx = {
    virtualHosts = {
      "krrk.skaven.org" = {
        forceSSL = true;
        enableACME = true;

        locations."/" = {
          proxyPass = "http://$krrk_upstream";
        };
      };

      "suwayomi.skaven.org" = {
        enableACME = true;
        forceSSL = true;

        globalRedirect = "krrk.skaven.org";
      };
    };

    appendHttpConfig = ''
      geo $krrk_upstream {
        default krrk_default_upstream;
        192.168.1.0/30 krrk_allowed_upstream;
        185.230.78.13/32 krrk_allowed_upstream;
        127.0.0.1/32 krrk_allowed_upstream;
      }
    '';

    upstreams = {
      krrk_default_upstream.servers = {
        "localhost:9000" = { };
      };
      krrk_allowed_upstream.servers = {
        "${config.networking.vpn-netns.vethIP}:4568" = { };
      };
    };
  };
}
