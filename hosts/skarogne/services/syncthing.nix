{ config, pkgs, ... }:

let
  port = 8384;
in

{
  users.users.syncthing.extraGroups = [
    "users"
    "medias"
  ];
  users.users.ratcornu.extraGroups = [ "syncthing" ];
  users.users.pantsushio.extraGroups = [ "syncthing" ];
  systemd.services.syncthing.serviceConfig.UMask = "0007";

  services.syncthing = {
    enable = true;
    enableVersioning = true;
    group = "medias";

    guiAddress = "0.0.0.0:${toString port}";
    openDefaultPorts = true;

    dataDir = "/var/lib/syncthing";

    settings = {
      gui = {
        theme = "black";
        user = "ratcornu";
        password = "$2a$10$cnNPmhPBBrHV8uB82co7sOr.x1mz/cELGlXyE6g8wrnWV/GKpWdoa";
      };
    };
  };

  systemd.tmpfiles.rules = [
    "d /home/ratcornu/musiques 2775 ratcornu ${config.services.syncthing.group}"
    "d /home/pantsushio/musiques 2775 pantsushio ${config.services.syncthing.group}"
    "d /home/polymorphe/musiques 2775 polymorphe ${config.services.syncthing.group}"
  ];

  services.nginx.virtualHosts = {
    "syncthing.skaven.org" = {
      forceSSL = true;
      enableACME = true;

      locations."/" = {
        proxyPass = "http://localhost:${toString port}";
      };
    };
  };
}
