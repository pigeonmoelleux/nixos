{
  config,
  pkgs,
  lib,
  ...
}:

{
  services.syncyomi = {
    # enable = true;
    package = pkgs.ratcornu.syncyomi;

    settings = {
      host = "localhost";
      DatabaseType = "postgres";
      PostgresHost = "localhost";
      PostgresPort = "5432";
      logMaxBackups = 10;
    };
  };

  services.nginx.virtualHosts."syncyomi.skaven.org" = {
    enableACME = true;
    forceSSL = true;

    locations."/" = {
      proxyPass = "http://${config.services.syncyomi.settings.host}:${toString config.services.syncyomi.settings.port}";
    };
  };
}
