{
  lib,
  config,
  options,
  ...
}:

let
  cfg = config.networking.vpn-netns;
in

{
  imports = [
    ./encapsulation.nix
    ./forwarding.nix
  ];

  options.networking.vpn-netns = with lib; {
    restrictedServices = mkOption {
      type = types.listOf types.str;
      default = [ ];
      description = "A list of valid systemd service names to be encapsulated in the vpn netns.";
    };

    nameserver = mkOption {
      type = types.singleLineStr;
      description = "The DNS server associated with the wireguard connection.";
    };

    wireguardInterface = mkOption {
      type = types.str;
      description = "The name of the wireguard interface.";
    };

    wireguardOptions = mkOption {
      type = with types; submodule { freeformType = attrsOf anything; };
      description = "Regular wireguard settings used to setup interface ${wgInterface}.";
    };

    interfaceNamespace = mkOption {
      type = types.singleLineStr;
      default = "vpn";
      description = "The name of the encapsulating netns.";
    };

    vethInterfaceName = mkOption {
      type = types.singleLineStr;
      default = "vethvpn";
      description = "The name of the veth interface accross netns.";
    };

    vethIP = mkOption {
      type = types.singleLineStr;
      default = "10.0.0.1";
      description = "The veth IP address of encapsulated services";
    };

    vethOuterIP = mkOption {
      type = types.singleLineStr;
      default = "10.0.0.2";
      description = "The veth IP address of non-encapsulated services.";
    };

    portForwarding = {
      enable = mkEnableOption "a port forwarding service.";

      leaseDuration = mkOption {
        type = types.int;
        default = 60;
        description = "The NATPMP lease duration in seconds.";
      };

      updateDuration = mkOption {
        type = types.int;
        default = 2;
        description = ''
          How long the update script takes (in seconds).
          This is substracted from the timer, so that leases do not get
          interrupted. Tweak this based on your hardware performance etc.
        '';
      };

      endpoint = mkOption {
        type = types.singleLineStr;
        default = cfg.nameserver;
        description = "The VPN endpoint (with which to negotiate the lease).";
      };

      temporaryPortRange = mkOption {
        type = options.networking.firewall.allowedUDPPortRanges.type.nestedTypes.elemType;
        default = {
          from = 30000;
          to = 30010;
        };
        description = ''
          The port range used for local port redirection. Make sure it doesn't
          interfere with other services, including the assignable port from your
          VPN provider.
        '';
      };
    };

    encapsulatedServices = mkOption {
      default = { };
      type =
        with types;
        attrsOf (submodule {
          options.enable = mkEnableOption "network namespace encapsulation for this service.";

          options.portForwarding = {
            enable = mkEnableOption "port forwarding for this service.";

            updateScript = mkOption {
              type = str;
              example = ''
                echo listenPort=$PORT > /var/lib/service/config.conf
              '';
              description = ''
                The script to apply everytime the forwarded port changes.
                The shell has access to the `$PORT` variable with the corresponding
                port. Be cautious, this script can perform arbitrary commands.
              '';
            };
          };
        });
    };
  };
}
