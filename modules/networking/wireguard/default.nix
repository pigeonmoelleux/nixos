{ config, lib, ... }:

let

  peers = import ./peers.nix;
  thisDevice = lib.lists.findSingle (peer: peer.name == config.networking.hostName) { } { } peers;
  otherDevices = lib.lists.remove thisDevice peers;

  cfg = config.networking.wireguard;
in

{
  config = {
    networking.wireguard = lib.mkIf cfg.enable {
      interfaces = {
        tunnels = {
          ips = thisDevice.ips;
          listenPort = 51818;

          privateKeyFile = "/etc/wireguard/private.key";
          peers = map (peer: lib.filterAttrs (n: _: n != "ips") peer) otherDevices;
        };
      };
    };
  };
}
