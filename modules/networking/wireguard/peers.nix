[
  {
    name = "skarogne";
    publicKey = "pyEsAEx0fpTDZVU838QVQ6Cg38ezUzthz+p6QVKG80o=";
    ips = [
      "10.13.0.1/24"
      "10.13.1.1/24"
      "10.13.2.1/24"
      "10.13.9.1/24"
    ];
    allowedIPs = [
      "10.13.0.0/16"
    ];
    endpoint = "185.230.78.13:51818";
    persistentKeepalive = 25;
  }
  {
    name = "skryre";
    ips = [ "10.13.9.2/24" ];
    publicKey = "Wxeh7pCM8XfgVHd48eoFj8pEKw27QS0kseM//ib2lEI=";
    allowedIPs = [ "10.13.9.2/32" ];
  }
  {
    name = "moulder";
    ips = [ "10.13.9.3/24" ];
    publicKey = "IvrTEyn83hTLZtdRDoyGmDscWBJ5PqFDSX6r70A7Giw=";
    allowedIPs = [ "10.13.9.3/32" ];
  }
  {
    name = "pestilens";
    ips = [ "10.13.9.4/24" ];
    publicKey = "DzuRVAinf+ISxsMl2EbZ3Mqq8PF5tlMuRLXEDcPUJ2E=";
    allowedIPs = [ "10.13.9.4/32" ];
  }
  {
    name = "volkn";
    ips = [ "10.13.9.5/24" ];
    publicKey = "WgTLOyhFzBFTCRhi6QyuWKDi4LjbmySnnZ5RQYq8+14=";
    allowedIPs = [ "10.13.9.5/32" ];
  }
  {
    name = "karak8";
    ips = [ "10.13.9.8/24" ];
    publicKey = "pmg2Hbcbxi6W3qsGX8GbwstfDelBxY7PdKKtPVt0LEk=";
    allowedIPs = [ "10.13.9.8/32" ];
  }
]
