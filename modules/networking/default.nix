{ ... }:

{
  imports = [
    ./vpn-netns
    ./wireguard
  ];

  networking = {
    nameservers = [
      "9.9.9.9"
      "1.1.1.1"
      "1.0.0.1"
    ];

    firewall = {
      checkReversePath = "loose";

      allowedTCPPorts = [ 13009 ];
      allowedUDPPorts = [ 51820 ];
    };
  };
  services.resolved.enable = true;
}
