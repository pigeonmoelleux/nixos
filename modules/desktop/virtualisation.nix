{ config, lib, ... }:

let
  cfg = config.desktop.virtualisation;

  inherit (lib)
    mkEnableOption
    mkIf
    mkOption
    types
    ;
in

{
  options.desktop.virtualisation = {
    enable = mkEnableOption "a default virtualisation environment.";

    waydroid.enable = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "Whether to enable waydroid.";
    };
  };

  config = mkIf cfg.enable {
    virtualisation = {
      libvirtd.enable = true;
      waydroid.enable = cfg.waydroid.enable;

      spiceUSBRedirection.enable = true;
    };

    programs.wireshark.enable = true;
    programs.virt-manager.enable = true;

    users.extraGroups.wireshark.members = [ "ratcornu" ];

  };
}
