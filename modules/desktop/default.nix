{ config, lib, ... }:

let
  cfg = config.desktop;

  inherit (lib) mkEnableOption mkIf mkDefault;
in

{
  imports = [
    ./input.nix
    ./games.nix
    ./gnome.nix
    ./virtualisation.nix
  ];

  options.desktop.enable = mkEnableOption "Full desktop environment.";

  config = mkIf cfg.enable {
    desktop = {
      input.enable = true;
      games = {
        steam.enable = mkDefault true;
        sunshine.enable = mkDefault true;
      };
      gnome.enable = mkDefault true;
      virtualisation.enable = mkDefault true;
    };
  };
}
