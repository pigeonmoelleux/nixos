{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.desktop.input;

  inherit (lib) mkEnableOption mkIf;
in

{
  options.desktop.input = {
    enable = mkEnableOption "the default input configuration.";
  };

  config = mkIf cfg.enable {
    i18n = {
      inputMethod = {
        enable = true;
        type = "ibus";
        ibus.engines = with pkgs.ibus-engines; [ anthy ];
      };
    };
  };
}
