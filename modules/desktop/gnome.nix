{ config, lib, ... }:

let
  cfg = config.desktop.gnome;

  inherit (lib) mkEnableOption mkIf;
in

{
  options.desktop.gnome = {
    enable = mkEnableOption "GNOME desktop";
  };

  config = mkIf cfg.enable {
    services.xserver = {
      desktopManager.gnome.enable = true;
      displayManager.gdm.enable = true;
    };

    services.gnome = {
      core-utilities.enable = false;
      localsearch.enable = false;
    };
  };
}
