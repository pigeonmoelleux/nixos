{ config, lib, ... }:

let
  cfg = config.desktop.games;

  inherit (lib)
    mkEnableOption
    mkIf
    mkOption
    types
    ;
in

{
  options.desktop.games = {
    steam = {
      enable = mkEnableOption "Steam gaming platform";
      vrSupport = mkOption {
        type = types.bool;
        default = false;
        example = true;
        description = "Whether to enable Steam VR support. (Currently not available)";
      };
    };

    sunshine.enable = mkEnableOption "Sunhine streaming platform";
  };

  config = {
    programs.steam = mkIf cfg.steam.enable {
      enable = true;

      localNetworkGameTransfers.openFirewall = true;
      protontricks.enable = true;
      remotePlay.openFirewall = true;
      gamescopeSession.enable = true;
    };

    services.sunshine = mkIf cfg.sunshine.enable {
      enable = true;

      autoStart = true;
      capSysAdmin = true;
      openFirewall = true;
    };
  };
}
