{ ... }:

{
  imports = [
    ./postgresql_backup.nix
    ./syncthing
  ];
}
