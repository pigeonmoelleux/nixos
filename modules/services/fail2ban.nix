{ config, lib, ... }:

let
  cfg = config.services.fail2ban;
in

{
  config = {
    services.fail2ban = lib.mkIf cfg.enable {
      ignoreIP = [
        "185.230.78.0/23"
        "10.13.0.0/16"
      ];

      maxretry = 4;
      bantime = "1h";
      bantime-increment = {
        enable = true;
        maxtime = "1y";
      };
    };

    services.openssh.settings.LogLevel = lib.mkIf cfg.enable "VERBOSE";
  };
}
