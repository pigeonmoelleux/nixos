{ lib, config, ... }:

let
  cfg = config.services.postgresqlBackup;
in

lib.mkIf cfg.enable {
  services.postgresqlBackup = {
    # Backup every database by default
    backupAll = lib.mkDefault true;

    # Backup everyday at an arbitrary loose hour
    startAt = lib.mkDefault "*-*-* 06:27:30";
  };
}
