{ config, lib, ... }:

let
  devices = import ./devices.nix;
  thisDevice = config.networking.hostName;

  allFolders = import ./folders.nix { inherit lib thisDevice; };
  filteredFolders = lib.filterAttrs (_: options: lib.elem thisDevice options.devices) allFolders;
  versionedFolders = lib.mapAttrs (
    _: options:
    {
      versioning = {
        type = "trashcan";
        params.cleanoutDays = "365";
      };
    }
    // options
  ) filteredFolders;

  folders = if cfg.enableVersioning then versionedFolders else filteredFolders;

  cfg = config.services.syncthing;
in

{
  options.services.syncthing = {
    enableVersioning = lib.mkEnableOption "folder versioning in Syncthing";
  };

  config = {
    assertions = [
      {
        assertion = with config.services.syncthing; enable -> user != "root";
        message = "syncthing: service may not be run with root";
      }
    ];

    services.syncthing = lib.mkIf cfg.enable {
      settings = {
        inherit devices folders;
      };

      overrideDevices = false;
      overrideFolders = false;
    };
  };
}
