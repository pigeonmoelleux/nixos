{ lib, thisDevice }:

let
  ratcornuDevices = [
    "skryre"
    "skarogne"
    "karak8"
    "mors"
    "moulder"
    "pestilens"
    "volkn"
    "vrrtkin"
  ];
  ratcornuDesktops = [
    "skryre"
    "vrrtkin"
  ];
  ratcornuServers = [ "skarogne" ];

  pantsushioDevices = [
    "pantsushio-pc-fixe"
    "pantsushio-pc-portable"
    "pantsushio-tel"
  ];
  polymorpheDevices = [
    "polymorphe-mac"
    "polymorphe-tel"
  ];
in

{
  ratcornu-administratif = {
    id = "ratcornu-administratif";
    devices = ratcornuDevices;
    path =
      if lib.skaven.isIn thisDevice ratcornuDesktops then
        "/home/ratcornu/Documents/administratif"
      else
        "/home/ratcornu/administratif";
    ignorePerms = true;
  };

  ratcornu-enseignement = {
    id = "ratcornu-enseignement";
    devices = ratcornuDevices;
    path =
      if lib.skaven.isIn thisDevice ratcornuDesktops then
        "/home/ratcornu/Documents/enseignement"
      else
        "/home/ratcornu/enseignement";
    ignorePerms = false;
  };

  ratcornu-musiques = {
    id = "ratcornu-musiques";
    devices = lib.remove "mors" ratcornuDevices;
    path = "/home/ratcornu/musiques";
    ignorePerms = true;
  };

  ratcornu-scolarite = {
    id = "ratcornu-scolarite";
    devices = ratcornuDevices;
    path =
      if lib.skaven.isIn thisDevice ratcornuDesktops then
        "/home/ratcornu/Documents/scolarite"
      else
        "/home/ratcornu/scolarite";
    ignorePerms = false;
  };

  pantsushio-musiques = {
    id = "rr6ig-ldihk";
    devices = pantsushioDevices ++ ratcornuServers;
    path = "/home/pantsushio/musiques";
    ignorePerms = true;
  };

  pantsushio-diffusion = {
    id = "9dnld-gubzs";
    devices = pantsushioDevices ++ ratcornuServers;
    path = "/home/pantsushio/diffusion";
    ignorePerms = true;
  };

  polymorphe-musiques = {
    id = "polymorphe-musiques";
    devices = polymorpheDevices ++ ratcornuServers;
    path = "/home/polymorphe/musiques";
    ignorePerms = true;
  };
}
