{ ... }:

{
  imports = [
    ./desktop
    ./networking
    ./services
    ./vfio.nix
  ];
}
