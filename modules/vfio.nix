{ config, lib, ... }:

let
  cfg = config.vfio;

  inherit (lib) types mkOption mkEnableOption;
in

{
  options.vfio = {
    enable = mkEnableOption "Configure the machine for VFIO";

    gpuIDs = mkOption {
      type = types.listOf types.str;
      description = "List of GPU IDs to use with VFIO";
    };
  };

  config = lib.mkIf cfg.enable {
    boot.kernelParams = [
      "vfio"
      "vfio_pci"
      "vfio_iommu_type1"
      ("vfio-pci.ids=" + lib.concatStringsSep "," cfg.gpuIDs)
    ];
  };
}
