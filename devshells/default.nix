{ mkDevShell, pkgs }:

with pkgs;
mkDevShell pkgs "nix" [
  nil
  nixpkgs-fmt
  nixpkgs-review
]
