{ mkDevShell, pkgs }:

with pkgs;
mkDevShell pkgs "C" [
  clang-tools
  llvmPackages_16.clangNoLibcxx
  lldb
]
