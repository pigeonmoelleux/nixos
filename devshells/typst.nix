{ mkDevShell, pkgs }:

with pkgs;
mkDevShell pkgs "typst" [
  typst
  typst-lsp
  typst-fmt
]
