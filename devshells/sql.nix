{ mkDevShell, pkgs }:

with pkgs;
mkDevShell pkgs "sql" [
  sqlite
  litecli
]
