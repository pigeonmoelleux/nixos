{ mkDevShell, pkgs }:

with pkgs;
mkDevShell pkgs "JS" [
  nodejs
  npm-lockfile-fix
]
