{ mkDevShell, pkgs }:

with pkgs;
with ocamlPackages;
mkDevShell pkgs "OCaml" [
  findlib
  batteries
  dune_3
  ocaml
  ocaml-lsp
  ocamlformat
  utop
]
