{ lib, ... }:

with builtins;
with lib;
{
  isIn = el: lst: any (x: x == el) lst;
}
