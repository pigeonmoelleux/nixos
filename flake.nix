{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "nixos-hardware";
    flake-parts.url = "github:hercules-ci/flake-parts";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    authentik = {
      url = "github:mayflower/authentik-nix";
      inputs = {
        flake-parts.follows = "flake-parts";
      };
    };
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    mailserver = {
      url = "gitlab:simple-nixos-mailserver/nixos-mailserver";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixpkgs-ratcornu.url = "github:RatCornu/nixpkgs/dev";
    spyware = {
      url = "github:RatCornu/spyware";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{
      self,
      nixpkgs,
      nixos-hardware,
      flake-parts,
      home-manager,
      agenix,
      authentik,
      fenix,
      mailserver,
      nixpkgs-ratcornu,
      spyware,
    }:
    let
      lib = nixpkgs.lib.extend (
        final: prev: {
          skaven = import ./lib {
            lib = final;
            inherit inputs;
          };
        }
      );
    in
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      flake = {
        nixosConfigurations =
          let
            overlay-ratcornu =
              final: prev:
              {
                ratcornu =
                  nixpkgs-ratcornu.legacyPackages.${prev.system}
                  // (import ./packages {
                    inherit inputs;
                    system = prev.system;
                  });
                spyware = spyware.defaultPackage."${prev.system}";
              }
              // (import ./overlays { inherit inputs prev final; });
          in
          {
            skryre = nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              inherit lib;
              modules = [
                (
                  { config, pkgs, ... }:
                  {
                    nixpkgs.overlays = [
                      overlay-ratcornu
                    ];
                  }
                )
                ./hosts
                ./hosts/skryre
                ./modules

                agenix.nixosModules.default
                home-manager.nixosModules.home-manager
              ];
              specialArgs = {
                inherit inputs;
              };
            };

            vrrtkin = nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              inherit lib;
              modules = [
                (
                  { config, pkgs, ... }:
                  {
                    nixpkgs.overlays = [
                      overlay-ratcornu
                    ];
                  }
                )

                ./hosts
                ./hosts/vrrtkin
                ./modules

                agenix.nixosModules.default
                home-manager.nixosModules.home-manager
              ];
              specialArgs = {
                inherit inputs;
              };
            };

            skarogne = nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              inherit lib;
              modules = [
                (
                  { config, pkgs, ... }:
                  {
                    nixpkgs.overlays = [
                      overlay-ratcornu
                    ];
                  }
                )
                ./hosts
                ./hosts/skarogne
                ./modules

                home-manager.nixosModules.home-manager
                agenix.nixosModules.default
                authentik.nixosModules.default
                mailserver.nixosModules.default
              ];
              specialArgs = {
                inherit inputs;
              };
            };

            karak8 = nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              inherit lib;
              modules = [
                (
                  { config, pkgs, ... }:
                  {
                    nixpkgs.overlays = [
                      overlay-ratcornu
                    ];
                  }
                )

                ./hosts
                ./hosts/karak8
                ./modules

                home-manager.nixosModules.home-manager
                agenix.nixosModules.default
              ];

              specialArgs = {
                inherit inputs;
              };
            };

            volkn = nixpkgs.lib.nixosSystem {
              system = "aarch64-linux";
              inherit lib;
              modules = [
                (
                  { config, pkgs, ... }:
                  {
                    nixpkgs.overlays = [
                      overlay-ratcornu
                    ];
                  }
                )
                ./hosts
                ./hosts/volkn
                ./modules

                agenix.nixosModules.default
                home-manager.nixosModules.home-manager
              ];
              specialArgs = {
                inherit inputs;
              };
            };
          };
      };

      perSystem =
        {
          config,
          pkgs,
          system,
          ...
        }:
        {
          packages = {
            home-ratcornu =
              (home-manager.lib.homeManagerConfiguration {
                pkgs = pkgs;
                lib = lib;

                modules = [
                  ./home
                ];

                extraSpecialArgs = {
                  inherit inputs;
                };
              }).activationPackage;

            home-ratcornu-graphical =
              (home-manager.lib.homeManagerConfiguration {
                pkgs = pkgs;
                lib = lib;

                modules = [
                  ./home
                  ./home/graphical
                ];

                extraSpecialArgs = {
                  inherit inputs;
                };
              }).activationPackage;
          } // (import ./packages { inherit inputs system; });

          devShells = lib.skaven.mapModulesNoDefault ./devshells (
            p: pkgs.callPackage p { inherit (lib.skaven) mkDevShell; }
          );
        };
    };
}
