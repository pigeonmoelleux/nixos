let
  ratcornu-skryre = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHa+ptSTNG4mnGUEGSkHTNDzyUGeiMnaWS2nDvJwrYTp ratcornu@skryre";
  skryre = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIE+O1tsTHqAyG9OWoAi8rWlTscw7LPtR7ywUhlqG5qGf root@skryre";

  ratcornu-vrrtkin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA41j5jdFj18OSHONx4QN9mMT+oBmtdwb1vstNavGOnz ratcornu@vrrtkin";
  vrrtkin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICmbAsuwqsxX3cjP73LHHH7jeColVFby4/jg7zBEDuva root@vrrkin";

  ratcornu-skarogne = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFKEhx3mbqXISnvBwI/XX1JOqdrIvSOgTzW7yUA6uCQR ratcornu@skarogne";
  skarogne = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINXGMzgigrXp4JTAonhwlV60bbLt5rxPWsH1fmSfY3cO root@skarogne";

  ratcornu-karak8 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaJNwqPVpPmFOJvt6D5kEFrS84Nquytir3lzZ8hwKF1 ratcornu@karak8";
  karak8 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEoPKNVFx/hjpsf8AYh6NcEweN6dh07Dr//aCQNW58RK root@karak8";

  owners = [
    ratcornu-skryre
    skryre
    ratcornu-vrrtkin
    vrrtkin
    ratcornu-skarogne
    skarogne
    ratcornu-karak8
    karak8
  ];

  secrets = [
    "karak8/services/suwayomi-pass"
    "skarogne/restic/local-env"
    "skarogne/restic/local-pass"
    "skarogne/restic/karak8-repo"
    "skarogne/restic/karak8-env"
    "skarogne/restic/karak8-pass"
    "skarogne/services/authentik-env-file"
    "skarogne/services/git-codeberg-token"
    "skarogne/services/git-poulet-token"
    "skarogne/services/grafana-secret"
    "skarogne/services/kanidm-idm-admin-password"
    "skarogne/services/kanidm-admin-password"
    "skarogne/services/ldap-admin-pass"
    "skarogne/services/lk-jwt-service-env"
    "skarogne/services/lldap-env-file"
    "skarogne/services/matrix-coturn-secret"
    "skarogne/services/matrix-extra-config"
    "skarogne/services/nextcloud-db-pass"
    "skarogne/services/nextcloud-pass"
    "skarogne/services/prowlarr-token"
    "skarogne/services/radarr-token"
    "skarogne/services/sonarr-token"
    "skarogne/services/spyware-env-file"
  ];
in

builtins.foldl' (
  acc: secret:
  acc
  // {
    "secrets/${secret}.age".publicKeys = owners;
  }
) { } secrets
