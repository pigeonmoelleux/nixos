{ pkgs, ... }:

{
  programs.git = {
    enable = true;
    lfs.enable = true;

    userName = "RatCornu";
    userEmail = "balthazar.patiachvili@crans.org";

    extraConfig = {
      init = {
        defaultBranch = "main";
      };

      user = {
        signingKey = "1B91F0873D061319D3D07F91FA47BDA260489ADA";
      };

      commit = {
        gpgSign = true;
      };

      core = {
        editor = "hx";
        excludesfile = builtins.toFile ".gitignore_global" ''
          .envrc
          .direnv/
          .vscode/
          result
          	      '';
      };

      safe = {
        directory = [
          "/etc/nixos"
          "/var/lib/gollum"
        ];
      };

      advice = {
        addignoredfile = false;
      };

      filter.lfs = {
        required = true;
        clean = "git-lfs clean -- %f";
        smudge = "git-lfs smudge -- %f";
        process = "git-lfs filter-process";
      };
    };
  };
}
