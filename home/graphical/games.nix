{ pkgs, ... }:

{
  home.packages = with pkgs; [
    (osu-lazer-bin.overrideAttrs (old: rec {
      version = "2024.906.2";
      src = builtins.fetchurl {
        url = "https://github.com/ppy/osu/releases/download/${version}/osu.AppImage";
        sha256 = "sha256:1ddnac72xk9zsi9pmxg72xd3piq6s5hgh7vzb09mh4r5mkfd22fd";
      };
    }))
  ];
}
