{ pkgs, lib, ... }:

{
  imports = [
    ./fonts
    ./games.nix
    ./ghostty.nix
    ./gnome.nix
    ./kdeconnect.nix
    ./notifications
    ./opam.nix
    ./tessen.nix
    ./vscode.nix
    ./xdg.nix
  ];

  home = {
    packages = with pkgs; [
      alvr
      appimage-run
      blackbox-terminal
      easyeffects
      element-desktop
      firefox
      fractal
      gapless
      joplin-desktop
      kooha
      libreoffice
      lshw
      mediainfo
      nicotine-plus
      (pass-wayland.withExtensions (
        ext: with pkgs.passExtensions; [
          pass-otp
        ]
      ))
      passff-host
      pavucontrol
      pinentry-gnome3
      polkit_gnome
      protonvpn-gui
      qbittorrent
      r2modman
      scrcpy
      signal-desktop
      tagger
      thunderbird
      usbimager
      vesktop
      vlc
      warp
      xournalpp
      zathura
    ];

    file = {
      ".mozilla/native-messaging-hosts/passff.json".source =
        "${pkgs.passff-host}/share/passff-host/passff.json";
      ".gnupg/gpg-agent.conf".text = "pinentry-program ${pkgs.pinentry-gnome3}/bin/pinentry-gnome3";
    };
  };

  nixpkgs.config.allowUnfreePredicate =
    pkg:
    builtins.elem (lib.getName pkg) [
      "discord"
      "vscode-extension-MS-python-vscode-pylance"
      "vscode-extension-ms-vscode-cpptools"
    ];

  programs.home-manager.enable = true;
}
