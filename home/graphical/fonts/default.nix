{ pkgs, ... }:

{
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    anthy
    font-awesome
    fira-code
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    liberation_ttf

    (pkgs.callPackage ./marianne.nix { })
    (pkgs.callPackage ./spectral.nix { })
  ];
}
