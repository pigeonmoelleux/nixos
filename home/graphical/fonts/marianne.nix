{
  pkgs,
  lib,
  stdenvNoCC,
  fetchurl,
}:

stdenvNoCC.mkDerivation rec {
  name = "marianne";
  version = "0.1.0";

  src = fetchurl {
    url = "https://www.systeme-de-design.gouv.fr/uploads/Marianne_fd0ba9c190.zip";
    hash = "sha256-2md27wId9ihr9leHiuPvjWZg37/DiuQrWk8FHn07+Gs=";
  };

  nativeBuildInputs = with pkgs; [
    unzip
  ];

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    ${pkgs.coreutils}/bin/mkdir -p $out/share/fonts/marianne $out/tmp
    ${pkgs.unzip}/bin/unzip $src -d $out/tmp/marianne
    ${pkgs.coreutils}/bin/mv "$out/tmp/marianne/Marianne/fontes desktop" $out/tmp/fonts
    find $out/tmp/fonts -name "*.otf" -exec install {} $out/share/fonts/marianne \;

    runHook postInstall
  '';
}
