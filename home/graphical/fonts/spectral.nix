{
  pkgs,
  ...
}:

pkgs.stdenvNoCC.mkDerivation {
  name = "spectral";
  version = "0-unstable-2024-11-18";

  src = pkgs.fetchFromGitHub {
    owner = "productiontype";
    repo = "Spectral";
    rev = "dbc06862d7030eedb1b01b60cdad8f6102f4ddfa";
    hash = "sha256-sEFJireK2KPA7fAJpibKM7XVUWm/SgJCHymhDGGLuko=";
  };

  nativeBuildInputs = with pkgs; [
    unzip
  ];

  dontUnpack = true;

  installPhase = ''
    runHook preInstall

    ${pkgs.coreutils}/bin/mkdir -p $out/share/fonts/spectral
    find $src -name "*.otf" -exec install {} $out/share/fonts/spectral \;
    find $src -name "*.woff2" -exec install {} $out/share/fonts/spectral \;

    runHook postInstall
  '';
}
