{ pkgs, ... }:

{
  programs.opam = {
    enable = true;
    enableBashIntegration = true;
  };
}
