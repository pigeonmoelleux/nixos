{ ... }:

{
  programs.ghostty = {
    enable = true;

    enableBashIntegration = true;

    settings = {
      theme = "GruvboxLight";
      font-size = 16;
    };
  };
}
