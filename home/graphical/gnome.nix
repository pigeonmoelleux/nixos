{ pkgs, ... }:

let
  xdg-terminal-exec = pkgs.writeScriptBin "xdg-terminal-exec" ''blackbox -c "$@"'';
in

{
  home.packages =
    with pkgs;
    [
      baobab
      eog
      evince
      file-roller
      gnome-calculator
      gnome-calendar
      gnome-clocks
      gnome-contacts
      gnome-disk-utility
      gnome-keyring
      gnome-mines
      gnome-system-monitor
      gnome-text-editor
      gnome-tweaks
      nautilus
      polari
      seahorse
      totem
      xdg-terminal-exec
    ]
    ++ (with pkgs.gnomeExtensions; [
      auto-move-windows
      only-window-maximize
      places-status-indicator
      tray-icons-reloaded
      vitals
    ]);
}
