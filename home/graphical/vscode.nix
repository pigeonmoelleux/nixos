{ pkgs, lib, ... }:

let
  vscode-utils = pkgs.vscode-utils;

  pinage404.better-readability-extension-pack = vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "better-readability-extension-pack";
      publisher = "pinage404";
      version = "4.0.0";
      hash = "sha256-JbwkaGv1zSG9fEejAAsOG4tCWSq3d4S+afZGNic+ylM=";
    };
  };
in

{
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;

    extensions =
      with pkgs.vscode-extensions;
      [
        arrterian.nix-env-selector
        bbenoist.nix
        dbaeumer.vscode-eslint
        dracula-theme.theme-dracula
        file-icons.file-icons
        haskell.haskell
        llvm-vs-code-extensions.vscode-clangd
        maximedenes.vscoq
        mkhl.direnv
        ms-ceintl.vscode-language-pack-fr
        ms-python.python
        ms-python.vscode-pylance
        myriad-dreamin.tinymist
        ocamllabs.ocaml-platform
        redhat.vscode-yaml
        rust-lang.rust-analyzer
        tamasfe.even-better-toml
        tomoki1207.pdf
        usernamehw.errorlens
        vadimcn.vscode-lldb
      ]
      ++ [
        pinage404.better-readability-extension-pack
      ];

    keybindings = [
      {
        key = "alt+a";
        command = "workbench.action.terminal.focus";
        when = "!terminalFocus";
      }
      {
        key = "alt+a";
        command = "workbench.action.focusActiveEditorGroup";
        when = "terminalFocus";
      }
      {
        key = "ctrl+alt+shift+i";
        command = "editor.action.inspectTMScopes";
      }
    ];

    userSettings = {
      "[c]" = {
        "editor.formatOnSave" = true;
      };
      "[coq]" = {
        "editor.tabSize" = 2;
      };
      "[latex]" = {
        "editor.formatOnSave" = false;
      };
      "[nix]" = {
        "editor.formatOnSave" = true;
        "editor.tabSize" = 2;
      };
      "[ocaml]" = {
        "editor.formatOnSave" = true;
        "editor.tabSize" = 2;
      };
      "[rust]" = {
        "editor.formatOnSave" = true;
      };
      "[typst]" = {
        "editor.formatOnSave" = true;
      };

      "files.associations" = {
        "*.v" = "coq";
        "*.mlg" = "ocaml";
        "*.mdln" = "madelaine";
        "*.mlw" = "ocaml";
      };

      "rust-analyzer.check.command" = "clippy";
      "rust-analyzer.cargo.features" = "all";

      "tinymist.exportPdf" = "onSave";
      "tinymist.formatterMode" = "typstyle";

      "window.zoomLevel" = 1;
      "redhat.telemetry.enabled" = false;

      "editor.minimap.enabled" = false;
    };
  };

  programs.direnv = {
    enable = true;

    nix-direnv.enable = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
  };
}
