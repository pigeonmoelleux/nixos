{ config, ... }:

{
  xdg = {
    enable = true;

    dataHome = "${config.home.homeDirectory}/.local/share";

    mimeApps = {
      enable = true;

      defaultApplications = {
        "inode/directory" = "org.gnome.Nautilus.desktop";
        "text/plain" = "org.gnome.TextEditor.desktop";
        "application/pdf" = "org.gnome.Evince.desktop";
        "video/vnd.avi" = "org.gnome.Totem.desktop";
      };
    };
  };
}
