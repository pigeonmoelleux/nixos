{
  pkgs,
  lib,
  config,
  ...
}:

let
  configFile = {
    action = "copy";
    pass_backend = "pass";
    dmenu_backend = "wofi";
    wofi_config_file = "${config.xdg.configHome}/wofi/config";
    userkey = "username";
    urlkey = "url";
    autotype_key = "password";
  };
in

{
  home.packages = with pkgs; [
    tessen
  ];

  programs.wofi = {
    enable = true;

    settings = {
      gtk_dark = true;
    };
  };

  home.file = {
    "${config.xdg.configHome}/tessen/config".text = lib.pipe configFile [
      (lib.mapAttrsToList (name: value: ''${name}="${toString value}"''))
      lib.concatLines
    ];
  };
}
