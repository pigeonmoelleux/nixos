{ pkgs, ... }:

{
  programs.neovim = {
    enable = true;

    viAlias = true;
    vimAlias = true;

    withPython3 = false;
    withRuby = false;

    extraConfig = ''
      set nocompatible

      set showmatch
      set ignorecase
      set smartcase
      set hlsearch
      set incsearch

      set tabstop=4
      set softtabstop=4
      set expandtab
      set shiftwidth=4
      set autoindent
      set nostartofline

      set number
      set relativenumber

      syntax on
      set ttyfast
      filetype plugin indent on

      set clipboard=unnamedplus
      set wildmode=longest,list
    '';

    plugins = with pkgs.vimPlugins; [
      vim-better-whitespace
      vim-lastplace
      vim-nix
      vim-code-dark
      nvim-lspconfig
      nvim-cmp
      cmp-nvim-lsp
      cmp_luasnip
      luasnip
      vimtex
    ];
  };
}
