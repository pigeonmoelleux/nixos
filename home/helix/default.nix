{ ... }:

{
  imports = [
    ./inputs.nix
  ];

  programs.helix = {
    enable = true;
    defaultEditor = true;

    settings = {
      theme = "gruvbox-light";

      editor = {
        cursorline = true;
        line-number = "relative";
        lsp.display-messages = true;
        true-color = true;
        soft-wrap.enable = true;

        inline-diagnostics = {
          cursor-line = "warning";
        };
      };

      keys = {
        normal = {
          C-s = ":w";
          L = "goto_line_end";
          H = "goto_line_start";
          X = [
            "extend_line_up"
            "extend_to_line_bounds"
          ];
        };
        select = {
          L = "goto_line_end";
          H = "goto_line_start";
          X = [
            "extend_line_up"
            "extend_to_line_bounds"
          ];
        };
      };
    };

    themes = {
      gruvbox-light = builtins.fromTOML (builtins.readFile ./gruvbox_light.toml);
    };

    languages = {
      language = [
        {
          name = "c";
          file-types = [
            "c"
            "h"
          ];
          auto-format = true;
        }
        {
          name = "git-commit";
          language-servers = [ "scls" ];
        }
        {
          name = "markdown";
          formatter = {
            command = "fold";
            args = [
              "-s"
              "-w80"
            ];
          };
        }
        {
          name = "nix";
          formatter = {
            command = "nixfmt";
          };
          auto-format = true;
        }
        {
          name = "ocaml";
          auto-format = true;
        }
        {
          name = "python";
          language-servers = [ "ruff-lsp" ];
          formatter = {
            command = "black";
            args = [
              "--quiet"
              "-"
            ];
          };
          auto-format = true;
        }
        {
          name = "rust";
          language-servers = [
            "rust-analyzer"
            "scls"
          ];
          auto-format = true;
        }
        {
          name = "typst";
          language-servers = [ "tinymist" ];
          formatter = {
            command = "typstyle";
          };
          auto-format = true;
        }
        {
          name = "unicode";
          scope = "text.unicode";
          file-types = [ ];
          shebangs = [ ];
          roots = [ ];
          language-servers = [ "scls" ];
          auto-format = false;
        }
      ];
      language-server = {
        ruff-lsp.command = "ruff-lsp";
        rust-analyzer = {
          config = {
            check = {
              command = "clippy";
            };
            cargo = {
              features = "all";
            };
          };
        };
        scls = {
          command = "simple-completion-language-server";
          config = {
            max_completion_items = 100;
            feature_words = true;
            feature_snippets = true;
            snippets_first = true;
            feature_unicode_input = true;
          };
        };
        tinymist = {
          command = "tinymist";
          config = {
            exportPdf = "onSave";
            outputPath = "$root/target/$dir/$name";
          };
        };
      };
    };
  };
}
