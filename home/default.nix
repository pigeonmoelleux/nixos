{
  inputs,
  pkgs,
  lib,
  ...
}:

let
  overlay-ratcornu = final: prev: {
    ratcornu =
      inputs.nixpkgs-ratcornu.legacyPackages.${prev.system} // (import ../packages { inherit pkgs; });
  };
in

{
  imports = [
    ./bash.nix
    ./bat.nix
    ./git.nix
    ./helix
    ./neovim.nix
    ./tmux.nix
  ];

  nixpkgs = {
    config.allowUnfree = true;
    overlays = [
      overlay-ratcornu
    ];
  };

  home = rec {
    username = "ratcornu";
    homeDirectory = "/home/${username}";
    stateVersion = "23.11";

    packages =
      with pkgs;
      [
        clang-manpages
        fd
        gcc
        gnumake
        graphviz
        hexpatch
        hexyl
        jq
        man-pages
        man-pages-posix
        mdl
        nil
        nixfmt-rfc-style
        pkg-config
        rclone
        ripgrep
        rustup
        screen
        smartmontools
        television
        typst
        typstyle
        tinymist
        xdg-utils
        zenith
        zip
      ]
      ++ (with pkgs.ratcornu; [
        lldap-cli
        simple-completion-language-server
      ]);
  };
}
