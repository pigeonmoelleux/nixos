{ pkgs, ... }:

{
  programs.bash = {
    enable = true;
    enableCompletion = true;

    bashrcExtra = ''
      export PS1="\[\e[0;37m\][\[\e[0;31m\]\u\[\e[0;37m\]@\[\e[0;32m\]\h\[\e[0;37m\]:\[\e[0;34m\]\w\[\e[0;37m\]]$ \[\e[0m\]"

      export PATH=$HOME/.local/bin/:$HOME/.local/bin:$PATH
      export PASSWORD_STORE_ENABLE_EXTENSIONS=true
      export HISTSIZE=10000
      export TERM=xterm-xfree86

      export GPG_TTY=$(tty)
      unset SSH_AGENT_PID
      export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

      export XDG_DATA_HOME="$HOME/.local/share"
    '';

    initExtra = ''
      madd () {
        yt-dlp --extract-audio --audio-format mp3 --audio-quality 0 -o "~/musiques/$1" $2
      }
    '';

    shellAliases = {
      ls = "ls --color=auto";
      l = "ls";
      ll = "ls -alh";
      la = "ls -a";
    };
  };

  programs.readline = {
    enable = true;
    bindings = {
      "\\e[A" = "history-search-backward";
      "\\e[B" = "history-search-forward";
    };
  };
}
