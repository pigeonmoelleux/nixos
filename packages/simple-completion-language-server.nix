{ pkgs, ... }:

pkgs.rustPlatform.buildRustPackage rec {
  pname = "simple-completion-language-server";
  version = "0-unstable-2025-01-21";

  src = pkgs.fetchFromGitHub {
    owner = "estin";
    repo = pname;
    rev = "05ce211f38387e1249e69abfd2fb4506fc624686";
    sha256 = "sha256-yP5ED3Y0VKMYWAWmMFpXEeJPGFjSVe3HZh0ek4W6OGg=";
  };

  cargoHash = "sha256-Zx8i4vkWRiI9xjPctBYvI4dvDP4VbxhHinoQ11ON//M=";

  doCheck = false;
}
