{ pkgs, ... }:

pkgs.suwayomi-server.overrideAttrs (old: rec {

  version = "1.1.1";
  revision = 1600;
  src = builtins.fetchurl {
    url = "https://github.com/Suwayomi/Suwayomi-Server-preview/releases/download/v${version}-r${toString revision}/Suwayomi-Server-v${version}-r${toString revision}.jar";
    sha256 = "sha256:074dfvllimwh6zs60dpm4s5s4bkxj2q382saklrrbxgzxc4fjwm7";
  };
})
