{ pkgs, ... }:

pkgs.stdenv.mkDerivation rec {
  pname = "lldap-cli";
  version = "0.1.0";

  src = pkgs.fetchFromGitHub {
    owner = "Zepmann";
    repo = pname;
    rev = "6eb61cef179696633cafe080a018cd085d3c3f64";
    sha256 = "sha256-Jchj4vqlGWmjFtdMwZAnI4VyBh+/p6rgZrpA77xlSb4=";
  };

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    install -m 755 $src/lldap-cli $out/bin/lldap-cli
    runHook postInstall
  '';

  buildInputs = with pkgs; [
    curl
    jq
    lldap
    patch
  ];
}
