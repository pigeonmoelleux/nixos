{ inputs, system }:

let
  pkgs = inputs.nixpkgs.legacyPackages.${system};
in

{
  lldap-cli = pkgs.callPackage ./lldap-cli.nix { inherit pkgs; };
  simple-completion-language-server = pkgs.callPackage ./simple-completion-language-server.nix {
    inherit pkgs;
  };
  suwayomi-server = pkgs.callPackage ./suwayomi-server.nix { inherit pkgs; };
}
